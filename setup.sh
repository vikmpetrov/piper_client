#!/bin/bash
echo Setting up Piper Client, please wait...
export PIPER=$PWD
export PIPER_OS=linux
source $PIPER/resources/linux/setup_python.command
source $PIPER/resources/linux/setup_nodejs.command
$PIPER/resources/linux/Python37/bin/python -m virtualenv venv_linux -p $PIPER/resources/linux/Python37/bin/python
source venv_linux/bin/activate
pip install -r requirements.txt
cd apps/launcher
npm install .
npm run package-linux
cd $PIPER
echo Piper Client setup completed.