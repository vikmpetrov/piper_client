# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
from piper_client.gui import Dialog
from piper_client.gui.qt import QtGui, QtCore, QtWidgets
from piper_client.gui.utils import error_message, warning_message, load_qt_ui
from piper_client.core.api import PiperClientApi
from piper_client.core.dcc import load_dcc_api
from piper_client.apps.publisher import Publisher


class CreateResource(Dialog):
    """
    A widget used to create resources and related version/file entities.
    """
    resource_created = QtCore.Signal(object)

    def __init__(self, parent):
        Dialog.__init__(self, parent, 'Piper Create Resource', 'create_resource.ui')
        dcc_api = load_dcc_api(dcc_name=self.parent.dcc.name)  # Get the parent DCC API.
        self.dcc_api = dcc_api(software=self.parent.dcc)
        self.file_prefix = None
        self.api = PiperClientApi()
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.contextLayout.addWidget(QtWidgets.QLabel('Project: {}'.format(self.parent.current_task.project.long_name)))
        entity_name = '{0}_{1}'.format(self.parent.current_task.link.sequence.name,
                                       self.parent.current_task.link.name)
        self.contextLayout.addWidget(QtWidgets.QLabel('Entity: {0}'.format(entity_name)))
        self.file_prefix = entity_name

        self.taskLineEdit.setText(self.parent.current_task.name)

        for file_extention in self.parent.dcc.file_extensions:
            self.fileExtentionComboBox.addItem(file_extention)

        return True

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.createResourceBtn.clicked.connect(self.create_workfile)
        self.cancelBtn.clicked.connect(self.close)

    def create_workfile(self):
        """
        Create a new resource and its initial version in the database.
        :return: False if the resource already exists, error message if the user and/or proposed name is invalid,
        otherwise - True;
        """
        resource_name = self.resourceNameLineEdit.text()

        # Sanity check the resource name:
        if not resource_name:
            return error_message(parent=self, msg='Please set a resource name in order to create it.')

        resource = self.api.create(entity_type='Resource',
                                   task=self.parent.current_task,
                                   name='{0}_{1}'.format(self.parent.current_task.name,
                                                         resource_name),
                                   source=self.parent.dcc.name,
                                   artist=self.parent.artist,
                                   description=self.descriptionLineEdit.text())

        if not resource:
            error_message(parent=self, msg='This resource already exists, creation failed.')
            return False

        os.environ['PIPER_RESOURCE_ID'] = str(resource.id)
        self.logger.info(msg='Set context resource ID: %s' % resource.id)
        publisher = Publisher(dcc_api=self.parent.dcc_api, ui=False, resource=resource)
        arg_dict = {'description': self.descriptionLineEdit.text(),
                    'is_work_file': True,
                    'for_dailies': False,
                    'selection_only': self.selectionCheckBox.isChecked(),
                    'file_format': self.fileExtentionComboBox.currentText()}
        publish = publisher.publish(arg_dict=arg_dict)
        if publish:
            self.resource_created.emit(self)
            notify = self.notifyLineEdit.text()
            if notify:
                recipients = [x for x in ' '.join(notify.split(',')).split() if x]
                if recipients:
                    subject = '{0} | Published resource {1}'.format(resource.project.long_name, resource.long_name),
                    message = 'Resource {0} has been published: \nURL: {1}\nPath: {2}'.format(resource.long_name,
                                                                                              resource.full_url,
                                                                                              resource.publish_path)
                    result = self.api.notify(subject=subject,
                                             message=message,
                                             recipients=','.join(recipients))
                    if result['warnings']:
                        warning_message(parent=self, msg='\n'.join(result['warnings']))
        self.close()
