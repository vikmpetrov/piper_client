# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import traceback
from piper_client.gui import Dialog
from piper_client.gui.qt import QtGui, QtCore, QtWidgets
from piper_client.gui.utils import info_message, error_message, load_qt_ui
from piper_client.core.api import PiperClientApi

ACTIVE_WINDOW = QtWidgets.QApplication.activeWindow()


class ExceptHook(Dialog):
    """
    A widget used to display exception errors.
    """

    def __init__(self, exctype, value, traceback):
        Dialog.__init__(self, ACTIVE_WINDOW, 'Piper Error', 'except_hook.ui')
        self.exctype = exctype
        self.value = value
        self.traceback = traceback
        self.api = PiperClientApi()
        self.artist = None
        self._setup_data()
        self._connect_widget_cmds()

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        error_msg = '# ' + '# '.join(traceback.format_exception(self.exctype, self.value, self.traceback)) + '\n'
        self.logger.error(error_msg)
        server_connection = True
        globals_config = None
        self.connectionLabel.setProperty('label_type', 'warning')
        self.emailConfigLabel.setProperty('label_type', 'warning')
        self.userEmailLabel.setProperty('label_type', 'warning')

        try:
            self.artist = self.api.get_current_artist()
            globals_config = self.api.globals
            self.connectionLabel.hide()
        except RuntimeError:
            server_connection = False

        if globals_config and globals_config.email_ready:
            self.emailConfigLabel.hide()
        else:
            self.submitBtn.setEnabled(False)

        if self.artist and self.artist.email:
            self.userEmailLabel.hide()
        else:
            self.submitBtn.setEnabled(False)

        self.errorTextEdit.insertPlainText('Server reached: {}\n'.format(server_connection))
        if self.artist:
            self.errorTextEdit.insertPlainText('\nUser: {}\n'.format(self.artist.username))
        self.errorTextEdit.insertPlainText('\nEnvironment:\n')
        user_environment = ''
        for key, value in os.environ.items():
            if key.startswith('PIPER_'):
                user_environment += '{0}: {1}\n'.format(key, value)
        self.errorTextEdit.insertPlainText(user_environment)
        self.errorTextEdit.insertPlainText('\nException:\n')
        self.errorTextEdit.insertPlainText(error_msg)
        
    def submit_ticket(self):
        """
        Submit an error ticket e-mail for the current exception.
        :return: None;
        """
        ticket_message = '{0}\nPIPER ERROR DETAILS:\n{1}'.format(self.descriptionTextEdit.toPlainText(),
                                                                 self.errorTextEdit.toPlainText())
        ticket_sent, result_message = self.api.send_ticket(message=ticket_message)
        if not ticket_sent:
            error_message(parent=self, msg='Error sending email: {}'.format(result_message))
        info_message(parent=self, msg=result_message)
        self.close()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.closeBtn.clicked.connect(self.close)
        self.submitBtn.clicked.connect(self.submit_ticket)
