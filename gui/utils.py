# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
from piper_client.gui.qt import QtCore, QtGui, QtWidgets, QtUiTools


def warning_message(parent, msg):
    """
    Shows a warning message.
    :param parent: the UI parent of this message;
    :param msg: the (str) message to be displayed;
    :return: True;
    """
    QtWidgets.QMessageBox.warning(parent, "Piper - Warning", msg)
    if hasattr(parent, 'logger'):
        parent.logger.warn(msg=msg)
    return True


def info_message(parent, msg):
    """
    Shows an information message.
    :param parent: the UI parent of this message;
    :param msg: the (str) message to be displayed;
    :return: True;
    """
    QtWidgets.QMessageBox.information(parent, "Piper - Info", msg)
    if hasattr(parent, 'logger'):
        parent.logger.info(msg=msg)
    return True


def error_message(parent, msg):
    """
    Show the critical message.
    :param parent: the UI parent of this message;
    :param msg: the (str) message to be displayed;
    :return: True;
    """
    QtWidgets.QMessageBox.critical(parent, "Piper - Error", msg)
    if hasattr(parent, 'logger'):
        parent.logger.error(msg=msg)
    return True


def confirm_dialog(parent, question):
    """
    Shows a confirmation dialog.
    :param parent: the UI parent of this message;
    :param question: the (str) question to be displayed;
    :return: True if confirmed, otherwise - False;
    """
    response = QtWidgets.QMessageBox.question(parent,
                                              "Piper - Confirm",
                                              question,
                                              QtWidgets.QMessageBox.Yes,
                                              QtWidgets.QMessageBox.No)
    if response == QtWidgets.QMessageBox.Yes:
        if hasattr(parent, 'logger'):
            parent.logger.debug(msg='"%s" confirmed!' % question)
        return True
    else:
        if hasattr(parent, 'logger'):
            parent.logger.debug(msg='"%s" rejected!' % question)
        return False


def set_combo_box_text(combo_box, text):
    """
    Attempt to set the text of a given combo box to some specific text.
    :param combo_box: the combo box to set the text of;
    :param text: the text to look for in the combo box's items (string);
    :return: None;
    """
    index = combo_box.findText(text, QtCore.Qt.MatchFixedString)
    if index >= 0:
        combo_box.setCurrentIndex(index)


def browse_file_dialog(parent, caption='Select file', filters=None):
    """
    Create a file browser dialog.
    :param parent: the parent Qt window/widget;
    :param caption: a (str) caption for the file browser dialog;
    :param filters: (str) file extension filters;
    :return: the (str) path of the selected file;
    """
    file_name, _ = QtWidgets.QFileDialog.getOpenFileNames(parent, caption, filter=filters)
    if hasattr(parent, 'logger'):
        parent.logger.debug(msg='Selected file: %s' % file_name)
    return file_name


def selection_dialog(parent, list_items, label='Select:'):
    """
    A dialog for selecting from a list of elements.
    :param parent: parent Qt window/widget;
    :param list_items: a list of items;
    :param label: a (str) label for the dialog;
    :return: the selected element;
    """
    selection, ok = QtWidgets.QInputDialog.getItem(parent=parent,
                                                   title='Select',
                                                   label=label,
                                                   items=list_items,
                                                   editable=False)
    return selection


class UiLoader(QtUiTools.QUiLoader):
    """
    A class for reading and loading a Qt UI form.
    """
    def __init__(self, base_instance):
        QtUiTools.QUiLoader.__init__(self, base_instance)
        self.base_instance = base_instance

    def createWidget(self, class_name, parent=None, name=''):
        if parent is None and self.base_instance:
            return self.base_instance
        else:
            # create a new widget for child widgets
            widget = QtUiTools.QUiLoader.createWidget(self, class_name, parent, name)
            if self.base_instance:
                setattr(self.base_instance, name, widget)
            return widget


def load_qt_ui(ui_file, base_instance=None):
    """
    Load a Qt UI form and return it as a widget.
    :param ui_file: the (str) path to the Qt UI form file;
    :param base_instance: a Qt base/parent UI class instance;
    :return: a Qt widget;
    """
    file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'forms', ui_file))
    loader = UiLoader(base_instance)
    widget = loader.load(file_path)
    QtCore.QMetaObject.connectSlotsByName(widget)
    return widget
