# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
from piper_client.config import ICONS_PATH, QSS_PATH
from piper_client.gui.qt import QtWidgets, QtGui
from piper_client.gui.utils import load_qt_ui
from piper_client.core.logger import get_logger


def _initialize_widget(widget, name, form):
    load_qt_ui(form, widget)
    widget.setStyle(QtWidgets.QStyleFactory.create('Plastique'))
    widget.setWindowIcon(QtGui.QIcon(os.path.join(ICONS_PATH, 'piper.png')))
    widget.setStyleSheet(open(os.path.join(QSS_PATH, 'style.qss')).read().replace('ICONS_PATH', ICONS_PATH))
    widget.setWindowTitle('{0} v{1}'.format(name, os.environ['PIPER_VERSION']))
    widget.logger = get_logger(name)


class MainWindow(QtWidgets.QMainWindow):
    """
    A Piper app base main window class.
    """

    def __init__(self, parent, name, form):
        QtWidgets.QMainWindow.__init__(self, parent)
        _initialize_widget(widget=self, name=name, form=form)
        self.parent = parent
        if self.parent:
            self.move(parent.rect().center() - self.rect().center())



class Dialog(QtWidgets.QDialog):
    """
    A Piper dialog base class.
    """

    def __init__(self, parent, name, form):
        QtWidgets.QDialog.__init__(self, parent)
        _initialize_widget(widget=self, name=name, form=form)
        self.parent = parent
        if self.parent:
            self.move(self.parent.rect().center())


class Widget(QtWidgets.QWidget):
    """
    A Piper widget base class.
    """

    def __init__(self, parent, name, form):
        QtWidgets.QWidget.__init__(self, parent)
        _initialize_widget(widget=self, name=name, form=form)
        self.parent = parent
