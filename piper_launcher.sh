#!/bin/bash
echo Starting Piper Launcher...
export PIPER=$PWD
export PIPER_TEMP=/tmp/
source $PIPER/piper_env.sh
python $PIPER/run.py