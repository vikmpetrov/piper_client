echo Setting up Piper Client, please wait...
@echo off
set PIPER=%~dp0
set PIPER_OS=win32
cd %PIPER%\resources\win32
call %PIPER%\resources\win32\setup_python.bat
call %PIPER%\resources\win32\setup_nodejs.bat
set PATH=%PIPER%\resources\win32\NodeJS;%PATH%
cd %PIPER%
%PIPER%\resources\win32\Python37\python.exe -m pip install virtualenv
%PIPER%\resources\win32\Python37\python.exe -m virtualenv venv_win32 -p %PIPER%resources\win32\Python37\python.exe
call %PIPER%\venv_win32\Scripts\activate.bat
pip install -r .\requirements.txt
cd %PIPER%\apps\launcher
%PIPER%\resources\win32\NodeJS\npm install . --scripts-prepend-node-path
%PIPER%\resources\win32\NodeJS\npm run package-win --scripts-prepend-node-path
cd %PIPER%
echo Piper Client setup completed.
pause