# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
from . import FarmJob as BaseFarmJob
from . import RENDER_CALLBACK_TEMPLATE
from piper_client.core.logger import get_logger
if 'QBDIR' in os.environ:
    sys.path.append('%s/api/python' % os.environ['QBDIR'])
import qb

LOGGER = get_logger('Piper Qube Render Farm API')


class FarmJob(BaseFarmJob):
    """
    Qube-based Piper render farm job class.
    """

    def __init__(self, version, file, farm, dcc_api, start_frame, end_frame, cpus, scene, flags='', by_frame=1,
                 priority=1, publish=True, retry_attempts=1, name='', description='', callback='', job_type='cmdrange'):
        super(FarmJob, self).__init__(version=version, file=file, farm=farm)
        # Setup qube job:
        render_flags = dcc_api.render_flags + flags
        self.command += '{0} {1} {2}'.format(dcc_api.software.command_line_renderer, render_flags)

        self.command = self.command.format(START_FRAME='QB_FRAME_START', END_FRAME='QB_FRAME_END',
                                           BY_FRAME='QB_FRAME_STEP', RENDER_NODE=dcc_api.render_node, SCENE=scene)

        frame_range = '{0}-{1}x{2}'.format(start_frame, end_frame, by_frame)

        self.job = {
            'name': name or self.version.long_name,
            'notes': description or self.version.description,
            'prototype': job_type,
            'priority': priority,
            'label': file.id,
            'cpus': cpus,
            'agenda': qb.genframes(frame_range),
            'callbacks': [],
            'package': {'cmdline': self.command},
            'retrywork': retry_attempts
        }

        additional_callback = ''
        if publish:
            additional_callback = 'piper_api.publish_from_id(entity_type="File", entity_id={FILE_ID});'
        piper_callback = RENDER_CALLBACK_TEMPLATE.format(ADDITIONAL_CALLBACK=additional_callback)
        piper_callback = piper_callback.format(FILE_ID=file.id)
        self.job['callbacks'].append(
            {
                'language': 'python',
                'triggers': 'done-job-self',
                'code': piper_callback
            })

        if callback:
            self.job['callbacks'].append(
                {
                    'language': 'python',
                    'triggers': 'done-job-self',
                    'code': callback
                })

    def submit(self):
        """
        Submit the job to the render farm.
        :return: True if successful, False otherwise;
        """
        LOGGER.info('Submitting command: %s' % self.command)
        submission = qb.submit(self.job)
        LOGGER.info('Farm job %s submitted!' % self.job['name'])
        return True
