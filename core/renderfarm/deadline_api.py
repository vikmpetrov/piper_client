# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
from shutil import copyfile
from datetime import datetime
from . import FarmJob as BaseFarmJob
from . import RENDER_CALLBACK_TEMPLATE
from piper_client.core.utils import run_in_terminal
from piper_client.core.logger import get_logger

LOGGER = get_logger('Piper Deadline Render Farm API')


class FarmJob(BaseFarmJob):
    """
    Deadline-based Piper render farm job class.
    """

    def __init__(self, version, file, farm, dcc_api, start_frame, end_frame, cpus, scene, flags='', by_frame=1,
                 priority=1, publish=True, retry_attempts=1, name='', description='', callback='', job_type=''):
        super(FarmJob, self).__init__(version=version, file=file, farm=farm)

        department = self.version.artist.department.long_name
        output_directory = os.path.dirname(file.work_path)
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)

        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')

        if publish:
            callback = 'piper_api.publish_from_id(entity_type="File", entity_id={FILE_ID});' + callback
        piper_callback = RENDER_CALLBACK_TEMPLATE.format(ADDITIONAL_CALLBACK=callback)
        piper_callback = piper_callback.format(FILE_ID=file.id)
        callback_file = os.path.join(output_directory, '_piper_deadline_callback_{}.py'.format(timestamp))
        with open(callback_file, 'w+') as tmp_file:
            tmp_file.write(piper_callback)

        props = '-prop Department="{DEPARTMENT}" -prop Comment="{DESCRIPTION}" -prop PostJobScript="{CALLBACK_FILE}"'

        command = '"{DEADLINE_COMMAND}" ' \
                  '-SubmitCommandLineJob ' \
                  '-executable "{EXECUTABLE}" ' \
                  '-arguments "{FLAGS}" ' \
                  '-frames {START_FRAME}-{END_FRAME}by{BY_FRAME} ' \
                  '-chunksize {CHUNK_SIZE} ' \
                  '-priority {PRIORITY} ' \
                  '-name "{JOB_NAME}" ' + props

        local_env_file = os.path.join(os.path.dirname(output_directory),
                                      '_piper_env_{0}.{1}'.format(timestamp, 
                                                                  os.environ['PIPER_ENV_FILE'].split('.')[-1]))

        copyfile(os.environ['PIPER_ENV_FILE'], local_env_file)
        self.command = self.command.replace(os.environ['PIPER_ENV_FILE'], local_env_file)
        os.environ['PIPER_ENV_FILE'] = local_env_file

        render_flags = '{0} {1}'.format(dcc_api.render_flags.format(START_FRAME='<STARTFRAME>',
                                                                    END_FRAME='<ENDFRAME>',
                                                                    BY_FRAME=by_frame,
                                                                    SCENE=scene,
                                                                    RENDER_NODE=dcc_api.software.render_node),
                                        flags)

        executable = '{0}"{1}"'.format(self.command,
                                       os.path.join(dcc_api.software.system.software_root,
                                                    dcc_api.software.command_line_renderer))
        if sys.platform == 'win32':
            executable += ' %*'
        else:
            executable += ' "$@"'

        extension = 'sh'
        if sys.platform == 'win32':
            extension = 'bat'
        executable_file = os.path.join(output_directory, '_piper_deadline_executable_{0}.{1}'.format(timestamp,
                                                                                                     extension))
        with open(executable_file, 'w+') as exec_file:
            exec_file.write(executable)

        self.command = command.format(DEADLINE_COMMAND=os.path.join(self.farm.system.software_root,
                                                                    self.farm.command_line_renderer),
                                      EXECUTABLE=executable_file,
                                      FLAGS=render_flags,
                                      SCENE=scene,
                                      START_FRAME=start_frame,
                                      END_FRAME=end_frame,
                                      BY_FRAME=by_frame,
                                      CHUNK_SIZE=int((end_frame-start_frame)/cpus) or 1,
                                      PRIORITY=priority,
                                      JOB_NAME=name or version.long_name,
                                      DEPARTMENT=department,
                                      DESCRIPTION=description,
                                      CALLBACK_FILE=callback_file)

    def submit(self):
        """
        Submit the job to the render farm.
        :return: True if successful, False otherwise;
        """
        LOGGER.info('Submitting command: %s' % self.command)
        run_in_terminal(command=self.command)
        LOGGER.info('Farm job submitted!')
        return True
