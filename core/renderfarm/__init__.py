# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import re
import importlib


RENDER_CALLBACK_TEMPLATE = """
import os, sys
sys.path.append(os.environ["PIPER_INIT"])
sys.path.append(os.environ["PIPER_SITE_PACKAGES"])
from piper_client.core.api import PiperClientApi

def __main__():
    piper_api = PiperClientApi()
    result = piper_api.request(url='rendered/File/{FILE_ID}', 
                               request_type='POST', 
                               data=dict(updated_by=piper_api.context_user_name))
    piper_api.logger.info(result)
    {ADDITIONAL_CALLBACK}

if __name__ == '__main__':
    __main__()
"""


class FarmJob(object):
    """
    The base farm job class.
    Contains variables and methods required to interact with render farm software.
    """

    def __init__(self, version, file, farm=None, platform=None):
        self.version = version
        self.file = file
        self.farm = farm
        if not platform:
            platform = self.farm.system.platform
        # Initialize the command to be run by the job based on the OS:
        if platform == 'win32':
            self.command = '"{}" && "%PIPER%\\piper_env.bat" && '
        elif platform == 'darwin':
            self.command = 'source {} && source $PIPER/piper_env.command && '
        else:
            self.command = 'source {} && source $PIPER/piper_env.sh && '
        self.command = self.command.format(os.environ['PIPER_ENV_FILE'])

    def submit(self):
        """
        Submit the job to the render farm.
        :return: submitted render job;
        """
        pass


def load_render_farm_api(render_farm_name):
    """
    Load an return a render farm-related API module.
    :param render_farm_name: the name of the related DCC;
    :return: a module object, e.g. core.dcc.qube_api.FarmJob;
    """
    api_name = '{}_api'.format(re.sub('[^A-Za-z]+', '', render_farm_name.lower()))
    module = importlib.import_module('piper_client.core.renderfarm.{}'.format(api_name))
    return module.FarmJob
