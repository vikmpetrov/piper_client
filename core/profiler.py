# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from piper_client.core.api import PiperClientApi


class Profiler(object):
    """
    An object to handle functionality related to software profiles.
    """
    def __init__(self):
        super(Profiler, self).__init__()
        self.api = PiperClientApi()
        self.profiles = dict()  # To store profile names and file paths.
        self.find_profiles()

    def find_profiles(self):
        """
        Find and store the names and paths to all profile files.
        :return: None
        """
        profiles = self.api.get(entity_type='Profile', fields=['modules', 'software'])
        for profile in profiles:
            self.profiles[profile.long_name] = profile
