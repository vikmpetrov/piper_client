# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import re
import importlib
from piper_client.core.api import PiperClientApi
from piper_client.core.logger import get_logger

LOGGER = get_logger('Piper DCC API')


class PiperDccApi(object):
    """
    The base Piper DCC API class.
    Contains variables and methods required to interact with a DCC.
    """

    name = None  # the DCC API name;
    src = None  # the native DCC API;
    render_flags = ''  # command line render flags;
    render_node = None  # name of the render node to be used for rendering;

    def __init__(self, software=None):
        self.api = PiperClientApi()
        if software:
            self.software = software
        else:
            self.software = self.api.get(entity_type='Software', entity_id=int(os.environ['PIPER_SOFTWARE_ID']),
                                         fields=['system', 'command_line_renderer'])

    def snapshot(self):
        """
        Save the current file as a snapshot file.
        :param self: the child DCC core module to use to run required helper functions;
        :return: None if the file is invalid, otherwise - the full name of the saved snapshot file;
        """
        current_file = self.get_current_file_name()
        if not current_file:
            LOGGER.debug('Current file is not valid for creating a snapshot.')
            return None
        file_name = current_file.replace('\\', '/').split('/')[-1]  # Get the name of the file only.
        file_extension = '.{}'.format(file_name.split('.')[-1])  # Get the file extension only.

        file_directory = current_file.replace(file_name, '')  # Get the file's directory only.

        # Check if we are in the '_snapshots' directory for this file:
        if '_snapshots' not in file_directory:
            # If not - append it to the current path and create it if it doesn't exist:
            snapshot_directory = os.path.join(file_directory, '_snapshots')
            if not os.path.isdir(snapshot_directory):
                LOGGER.debug('Creating snapshot directory %s' % snapshot_directory)
                os.makedirs(snapshot_directory)
        else:
            snapshot_directory = file_directory

        # Check if the current file is a snapshot file based on its naming
        # and determine what part of the file name to replace:
        if '-snap-' in file_name:
            replace = '-snap-{}'.format(file_name.split('-snap-')[-1])
        else:
            replace = file_extension

        # Check for existing snapshot files and create the name for the new snapshot file:
        existing_snapshots = [x for x in os.listdir(snapshot_directory) if x.startswith(file_name)]
        new_snapshot = file_name.replace(replace,
                                         '-snap-{0}{1}'.format(str(len(existing_snapshots)+1).zfill(4), file_extension))

        snapshot_file = os.path.join(snapshot_directory, new_snapshot)
        LOGGER.debug('Creating snapshot %s' % snapshot_file)
        return self.save_file(snapshot_file)

    def create_publish_group(self):
        """
        Create publishing group.
        :return: a group node;
        """
        selection = self.get_selection()
        if not selection:
            self.select_all()
        group = self.create_group()
        return group

    def save_work_file(self, file_entity, selection_only=False):
        """
        Save the current file as a work file.
        :param file_entity: the file entity that represents the work file;
        :param selection_only: (bool) whether to publish from the current selection;
        :return: None;
        """
        if not selection_only:
            self.select_all()
        group = self.create_publish_group()
        group = self.tag_publish_node(node=group, file_entity=file_entity)
        # Export the group as the published file:
        self.select(group)
        self.export_selection(file_path=file_entity.work_path)
        self.clear_selection()

    def open_file(self, path_to_file):
        """
        Open a specific file.
        :param path_to_file: the full path to the file to open;
        :return: the full name of the opened file as a string if it exists, otherwise - None;
        """
        pass

    def save_file(self, file_path=None):
        """
        Save a file to a specified path.
        :param file_path: the path to save the file to;
        :return: the full name of the saved file as a string;
        """
        pass

    def export_selection(self, file_path):
        """
        Export the current selection to the given fil path.
        :param file_path: the path to export the file to;
        :return: the full name of the saved file as a string;
        """
        pass

    def get_current_file_name(self):
        """
        Get the full path of the file currently opened.
        :return: the full path of the current file (string);
        """
        pass

    def get_current_file_format(self):
        """
        Get the current file's format.
        :return: a (str) file format;
        """
        pass

    def find_node_from_id(self, id_attribute, id_value, nodes=None):
        """
        Find a node in a list of nodes, given a ID attribute and its value.
        :param id_attribute: the (str) ID attribute to look for;
        :param id_value: the value of the ID attribute to match;
        :param nodes: a list of nodes to look through;
        :return: a node with a matching attribute/value pair if it exists, otherwise None;
        """
        pass

    def select(self, node):
        """
        Select a given node.
        :param node: a scene node;
        :return: None;
        """
        pass

    def select_all(self):
        """
        Select all nodes.
        :return: None;
        """
        pass

    def add_to_selection(self, node):
        """
        Add the a given node to the current selection.
        :param node: a scene node;
        :return: None;
        """
        pass

    def get_selection(self):
        """
        Get a list of the currently selected nodes.
        :return: a list of nodes;
        """
        pass

    def clear_selection(self):
        """
        Clear all selection in the scene.
        :return: None;
        """
        pass

    def ungroup(self, group):
        """
        Delete or ungroup a given group node name.
        :param group: a group node;
        :return: None
        """
        pass

    def create_group(self):
        """
        Group nodes together into a single node.
        :return: a group node;
        """
        pass

    def tag_publish_node(self, node, file_entity):
        """
        Set the publish group node's ID attributes to correspond to the relevant file entity.
        :param node: the group node;
        :param file_entity: the published file entity;
        :return: :return: the tagged publish node;
        """
        pass

    def add_published_file(self, file_entity):
        """
        Add a published file to the scene.
        :param file_entity: the published file entity to add;
        :return: None;
        """
        pass

    def find_entity_nodes(self, root=None):
        """
        Find all nodes representing entities in the scene under a given root.
        :param root: a root entity node to start the search from;
        :return: a dictionary of published file ID values for the given root key;
        """
        pass

    def remove_published_file(self, file_entity):
        """
        Remove a published file from the scene.
        :param file_entity: the file entity representing the file to be removed;
        :return: None;
        """
        pass

    def replace_published_file(self, previous_file_entity, new_file_entity):
        """
        Replace an existing published file node with a new published file node.
        :param previous_file_entity: the version file entity of the previously selected version;
        :param new_file_entity: the newly selected file entity be added;
        :return: True if successful, otherwise False;
        """
        pass

    def get_render_start_frame(self):
        """
        Get the start frame value as set in the render settings.
        :return: (float) start frame;
        """
        pass

    def get_render_end_frame(self):
        """
        Get the end frame value as set in the render settings.
        :return: (float) end frame;
        """
        pass

    def set_render_start_frame(self, value):
        """
        Set the render start frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        pass

    def set_render_end_frame(self, value):
        """
        Set the render end frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        pass

    def get_playback_start_frame(self):
        """
        Get the start frame value as set in the current scene playback range.
        :return: (float) start frame;
        """
        pass

    def get_playback_end_frame(self):
        """
        Get the end frame value as set in the current scene playback range.
        :return: (float) end frame;
        """
        pass

    def set_playback_start_frame(self, value):
        """
        Set the playback start frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        pass

    def set_playback_end_frame(self, value):
        """
        Set the render end frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        pass

    def get_image_format(self):
        """
        Get the output image format as set in the render settings.
        :return: (str) image format or empty string if unavailable;
        """
        pass

    def get_fps(self):
        """
        Get the current FPS value.
        :return: (float) frames per second value;
        """
        pass

    def set_fps(self, value):
        """
        Set the scene's FPS to the given value.
        :param value: the FPS number value;
        :return: None;
        """
        pass

    def set_render_path(self, path):
        """
        Set a specific render path in the render settings.
        :param path: a (str) path;
        :return: None;
        """
        pass

    def list_nodes(self, node_type):
        """
        List the full names of all scene nodes of a certain type, the default being render node type.
        :param node_type: the (str) type of node to list;
        :return: a list of nodes;
        """
        pass

    def render_setup(self, file_entity, render_node=None):
        """
        Setup the current file so that it's ready for rendering.
        :param file_entity: the current file entity;
        :param render_node: a render node to use in the render setup instead of creating a new one;
        :return: None;
        """
        pass


def load_dcc_api(dcc_name):
    """
    Load an return a DCC-related API module.
    :param dcc_name: the name of the related DCC;
    :return: a module object, e.g. core.dcc.maya_api.PiperDccApi;
    """
    dcc_api_name = '{}_api'.format(re.sub('[^A-Za-z]+', '', dcc_name.lower()))
    dcc_module = importlib.import_module('piper_client.core.dcc.{}'.format(dcc_api_name))
    return dcc_module.PiperDccApi
