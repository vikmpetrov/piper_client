# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
from maya import cmds, mel
from tempfile import gettempdir
from piper_client.core.dcc import PiperDccApi as BaseApi
from piper_client.core.logger import get_logger

LOGGER = get_logger('Piper Maya API')


class PiperDccApi(BaseApi):
    """
    Maya Piper API.
    """

    name = 'maya'
    src = cmds
    render_flags = '-s {START_FRAME} -e {END_FRAME} -b {BY_FRAME} {SCENE}'
    render_node = None

    def __init__(self, **kwargs):
        super(PiperDccApi, self).__init__(**kwargs)

    def open_file(self, path_to_file):
        """
        Open a specific file.
        :param path_to_file: the full path to the file to open;
        :return: the full name of the opened file as a string if it exists, otherwise - None;
        """
        if os.path.isfile(path_to_file):
            LOGGER.debug('Opening file %s' % path_to_file)
            return cmds.file(path_to_file, open=True, force=True)
        else:
            LOGGER.debug('Could not find file %s' % path_to_file)
            return None

    def _file_type_from_path(self, file_path):
        if file_path and file_path.lower().endswith('.ma'):
            return 'mayaAscii'
        return 'mayaBinary'

    def save_file(self, file_path=None):
        """
        Save the current file.
        :param file_path: the path to save the file to;
        :return: the full name of the saved file as a string;
        """
        if not file_path:
            file_path = self.get_current_file_name()
            if not file_path:
                file_path = os.path.join(gettempdir(), 'temp.ma')
        file_type = self._file_type_from_path(file_path=file_path)
        try:
            cmds.file(rename=file_path)
            LOGGER.debug('Saving file %s' % file_path)
            return cmds.file(save=True, type=file_type)
        except RuntimeError as e:
            LOGGER.error('There was a problem trying to save file %s: %s' % (file_path, e))
            return False

    def export_selection(self, file_path):
        """
        Export the current selection to the given fil path.
        :param file_path: the path to export the file to;
        :return: the full name of the saved file as a string;
        """
        file_type = self._file_type_from_path(file_path=file_path)
        try:
            LOGGER.debug('Exporting selection to file %s' % file_path)
            return cmds.file(file_path, es=True, type=file_type)
        except RuntimeError as e:
            LOGGER.error('There was a problem trying to export the current selection to file %s: %s' % (file_path, e))
            return False

    def snapshot(self):
        """
        Save the current file as a snapshot file.
        :return: None if the file is invalid, otherwise - the full name of the saved snapshot file;
        """
        snap = super(PiperDccApi, self).snapshot()
        if snap:
            cmds.confirmDialog(title='Piper Snapshot', message='Snapshot created!', icon='information')
        else:
            cmds.confirmDialog(title='Piper Snapshot', message='The current file is invalid for a snapshot!',
                               icon='error')
        return snap

    def get_current_file_name(self):
        """
        Get the full path of the file currently opened.
        :return: the full path of the current file (string);
        """
        return cmds.file(q=True, sn=True)

    def get_current_file_format(self):
        """
        Get the current file's format.
        :return: a (str) file format;
        """
        file_name = self.get_current_file_name()
        if not file_name:
            file_format = 'mb'
        else:
            file_format = file_name.split('.')[-1]
        return file_format

    def find_node_from_id(self, id_attribute, id_value, nodes=None):
        """
        Find a node in a list of nodes, given a ID attribute and its value.
        :param id_attribute: the (str) ID attribute to look for;
        :param id_value: the value of the ID attribute to match;
        :param nodes: a list of nodes to look through;
        :return: a node with a matching attribute/value pair if it exists, otherwise None;
        """
        # If no nodes specified, recurse through all group nodes:
        if not nodes:
            nodes = cmds.ls(type='transform', absoluteName=True)
        matching_node = None
        for node in nodes:
            # Look for the attribute and check its value:
            if cmds.attributeQuery(id_attribute, node=node, exists=True):
                if cmds.getAttr('{0}.{1}'.format(node, id_attribute)) == id_value:
                    matching_node = node
                    break
    
        return matching_node

    def select(self, node):
        """
        Select a given node.
        :param node: a scene node;
        :return: None;
        """
        cmds.select(node)

    def select_all(self):
        """
        Select all nodes.
        :return: None;
        """
        cmds.select(all=True)

    def add_to_selection(self, node):
        """
        Add the a given node to the current selection.
        :param node: a scene node;
        :return: None;
        """
        cmds.select(node, add=True)

    def get_selection(self):
        """
        Get a list of the currently selected nodes.
        :return: a list of nodes;
        """
        return cmds.ls(sl=True, fl=True)

    def clear_selection(self):
        """
        Clear all selection in the scene.
        :return: None;
        """
        cmds.select(clear=True)

    def ungroup(self, group):
        """
        Delete or ungroup a given group node name.
        :param group: a (str) group node name;
        :return: None
        """
        if not cmds.listRelatives(group, children=True):
            cmds.delete(group)
        else:
            cmds.ungroup(group)  # versions are only grouped in publish files;

    def create_group(self):
        """
        Group nodes together into a single node.
        :return: a group node;
        """
        try:
            group = cmds.group()
        except RuntimeError:
            group = cmds.group(empty=True)
        return group

    def tag_publish_node(self, node, file_entity):
        """
        Add a 'piper_file_id' attribute to a given node and set its value to the value of the given file entity ID.
        :param node: the group node;
        :param file_entity: the published file entity;
        :return: the tagged publish node;
        """
        original_selection = cmds.ls(sl=True, fl=True)
        cmds.select(node)
        cmds.addAttr(longName='piper_file_id', hidden=True, dataType='string')
        cmds.setAttr('{}.piper_file_id'.format(node), str(file_entity.id), type='string')
        for attr in ['tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz']:
            cmds.setAttr('{0}.{1}'.format(node, attr), lock=True, channelBox=False, type='string')
        cmds.select(original_selection)
        if file_entity.name not in node:
            node = cmds.rename(node, file_entity.name)
        return node

    def add_published_file(self, file_entity):
        """
        Add a published file to the scene.
        :param file_entity: the published file entity to add;
        :return: None;
        """
        if file_entity.format in self.software.file_extensions:
            cmds.file(file_entity.publish_path, reference=True, namespace='published', mergeNamespacesOnClash=True)
        else:
            group = 'published:{}'.format(file_entity.name)
            cmds.file(file_entity.publish_path, i=True, groupReference=True, groupName=group)
            self.tag_publish_node(node=group, file_entity=file_entity)

    def find_entity_nodes(self, root=None, root_nodes=None):
        """
        Find all nodes representing entities in the scene under a given root.
        :param root: a root entity node to start the search from;
        :param root_nodes: a list of root entity nodes to start the search from;
        :return: a dictionary of published file ID values for the given root key;
        """
        found_ids = {}
        if root and not root_nodes:
            root_node = self.find_node_from_id(id_attribute='piper_file_id', id_value=str(root.id))
            if not root_node:
                return found_ids
            root_nodes = cmds.listRelatives(root_node, children=True, fullPath=True)
        elif not root and not root_nodes:
            root_nodes = cmds.ls(assemblies=True, absoluteName=True)

        if root_nodes:
            for node in root_nodes:
                if cmds.attributeQuery('piper_file_id', node=node, exists=True):
                    piper_file_id = int(cmds.getAttr('{}.piper_file_id'.format(node)))
                    found_ids[piper_file_id] = {}
                    children = cmds.listRelatives(node, children=True, fullPath=True)
                    if children:
                        found_ids[piper_file_id] = self.find_entity_nodes(root_nodes=children)

        return found_ids

    def remove_published_file(self, file_entity):
        """
        Remove a published file from the scene.
        :param file_entity: the file entity representing the file to be removed;
        :return: None;
        """
        for reference in cmds.ls(type='reference', absoluteName=True):
            try:
                if cmds.referenceQuery(reference, f=True) == file_entity.publish_path:
                    cmds.file(removeReference=True, referenceNode=reference)
                    return
            except RuntimeError:
                continue
        node = self.find_node_from_id(id_attribute='piper_file_id', id_value=str(file_entity.id))
        if node:
            cmds.delete(node)

    def replace_published_file(self, previous_file_entity, new_file_entity):
        """
        Replace an existing published file node with a new published file node.
        :param previous_file_entity: the version file entity of the previously selected version;
        :param new_file_entity: the newly selected file entity be added;
        :return: True if successful, otherwise False;
        """
        # Find the target node to replace:
        target_node = self.find_node_from_id(id_attribute='piper_file_id',
                                             id_value=str(previous_file_entity.id))
        if not target_node:
            return False

        if new_file_entity.format.lower() in ['ma', 'mb']:
            reference_node = cmds.referenceQuery(target_node, referenceNode=True)
            cmds.file(new_file_entity.publish_path, loadReference=reference_node)
        else:
            cmds.delete(target_node)
            self.add_published_file(file_entity=new_file_entity)
    
        return True

    def get_render_start_frame(self):
        """
        Get the start frame value as set in the render settings.
        :return: (float) start frame;
        """
        return int(mel.eval('getAttr("defaultRenderGlobals.startFrame")'))

    def get_render_end_frame(self):
        """
        Get the end frame value as set in the render settings.
        :return: (float) end frame;
        """
        return int(mel.eval('getAttr("defaultRenderGlobals.endFrame")'))

    def set_render_start_frame(self, value):
        """
        Set the render start frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        mel.eval('setAttr("defaultRenderGlobals.startFrame", {})'.format(value))

    def set_render_end_frame(self, value):
        """
        Set the render end frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        mel.eval('setAttr("defaultRenderGlobals.endFrame", {})'.format(value))

    def get_playback_start_frame(self):
        """
        Get the start frame value as set in the current scene playback range.
        :return: (float) start frame;
        """
        return cmds.playbackOptions(q=True, minTime=True)

    def get_playback_end_frame(self):
        """
        Get the end frame value as set in the current scene playback range.
        :return: (float) end frame;
        """
        return cmds.playbackOptions(q=True, maxTime=True)

    def set_playback_start_frame(self, value):
        """
        Set the playback start frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        cmds.playbackOptions(minTime=value)

    def set_playback_end_frame(self, value):
        """
        Set the render end frame to the given frame value.
        :param value: the frame number value;
        :return: None;
        """
        cmds.playbackOptions(maxTime=value)

    def get_image_format(self):
        """
        Get the output image format as set in the render settings.
        :return: (str) image format or empty string if unavailable;
        """
        image_name = mel.eval('renderSettings -firstImageName -fullPath')
        return image_name[0].split('.')[-1]

    def get_fps(self):
        """
        Get the current FPS value.
        :return: (float) frames per second value;
        """
        return cmds.playbackOptions(q=True, fps=True)

    def set_fps(self, value):
        """
        Set the scene's FPS to the given value.
        :param value: the FPS number value;
        :return: None;
        """
        cmds.playbackOptions(fps=value)

    def set_render_path(self, path):
        """
        Set a specific render path in the render settings.
        :param path: a (str) path;
        :return: None;
        """
        cmds.workspace(fileRule=['images', path])

    def list_nodes(self, node_type=''):
        """
        List the full names of all scene nodes of a certain type, the default being render node type.
        :param node_type: the (str) type of node to list;
        :return: a list of node names;
        """
        return cmds.ls(type=node_type)

    def render_setup(self, file_entity, render_node=None):
        """
        Setup the current file so that it's ready for rendering.
        :param file_entity: the current file entity;
        :param render_node: a render node to use in the render setup instead of creating a new one;
        :return: None;
        """
        self.set_render_path(path=file_entity.work_path)
