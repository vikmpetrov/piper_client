# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""
__version__ = '1.0.1'


import os
import sys
import requests
import webbrowser
from piper_client.core.models import *
from piper_client.core.logger import get_logger
from piper_client.core.utils import get_current_user_name, json_to_string, string_to_json, environment_check
from piper_client.config import credentials_config

LOGGER = get_logger('Piper Client API')


class PiperClientApi:
    """Client-side API for interacting with the Piper Server."""

    def __init__(self):
        self.server = None  # to store the Piper Server URL;
        self.auth = None  # to contain authentication (user, password) tuple;
        self.query_fields = []  # specific fields to query when getting entities from the database;
        self.query_fields_reset = True  # reset query fields after each query;
        self._globals = None  # to store the studio globals entity;
        self._artist = None  # to store the current artist entity;
        self._system = None  # to store the current system entity;
        self.user_name = get_current_user_name()
        self.reload(refresh=False)

    @property
    def context_user_name(self):
        username = self.user_name
        if 'PIPER_USER' in os.environ:
            username = os.environ['PIPER_USER']
        return username

    def reload(self, refresh=True):
        """
        Reload all key Piper Client API variables.
        :param refresh: (bool) reload the studio config too;
        :return: None;
        """
        if refresh:
            credentials_config.reload()
        self.server = '{}/api'.format(os.environ['PIPER_SERVER'])
        if not self.server.startswith('http://') or self.server.startswith('https://'):
            self.server = 'http://' + self.server
        if 'piper_username' in credentials_config.data:
            self.auth = (credentials_config.data['piper_username'], credentials_config.data['piper_password'])

    def authorize(self, username, password, silent=False):
        """
        Try to authorize a given username and password on the Piper Server.
        :param username: a (str) username;
        :param password: a (str) password;
        :param silent: (bool) whether to produce an error if unsuccessful;
        :return: True/False;
        """
        previous_auth = self.auth
        if username and password:
            self.auth = (username, password)
            test = self.get(entity_type='GlobalsConfig')
            if test.status_code != 200:
                self.auth = previous_auth
            else:
                return True
        if not silent:
            raise RuntimeError('Invalid username and/or password provided.')
        return False

    @staticmethod
    def _url_sanity_check(url):
        """
        Ensure an URL does not start with a slash.
        :param url: a (str) URL;
        :return: a (str) URL, corrected if required;
        """
        if url.startswith('/'):
            url = url[1:]
        return url

    def _gather_request_data(self, attribute=None, filters={}, fields=[]):
        """
        Consolidate all request data into a JSON package to be sent to the Piper Server.
        :param attribute: a (str) attribute to query;
        :param filters: a list of filters to apply to the query;
        :param fields: a list of fields to query when getting entities from the database;
        :return: a request data dictionary;
        """
        data = dict()
        if filters:
            for key, value in filters.items():
                if key == 'parent':
                    data['_parent_id'] = value.id
                    data['_parent_model'] = value.type
                    continue
                if issubclass(type(value), Entity):
                    data[key] = json_to_string(value.data)
                else:
                    data[key] = value
        fields = fields or self.query_fields
        if fields:
            data['_fields'] = ','.join(fields)
        if attribute:
            data['attribute'] = attribute
        return data

    def get(self, entity_type, entity_id=None, url=None, attribute=None, filters={}, fields=[]):
        """
        Get entity/entities from the Piper Server.
        :param entity_type: the (str) type of entity to get;
        :param entity_id: a specific (int) ID of the target entity to get;
        :param url: a specific URL to access;
        :param attribute: the (str) name of a specific attribute to query;
        :param filters: a list of filters to apply during the query;
        :param fields: a list of fields to query when getting entities;
        :return: an entity, list of entities or attribute value depending on the request;
        """
        # Use a default URL if one is not provided:
        if not url:
            url = entity_type
            if entity_id:
                url += '/{}'.format(entity_id)

        # Consolidate request data into a JSON dictionary:
        data = self._gather_request_data(attribute=attribute, filters=filters, fields=fields)

        LOGGER.debug(msg='Requesting (GET): %s' % url)

        # Make a request to the Piper Server:
        try:
            request = requests.get('{0}/{1}/'.format(self.server, url), auth=self.auth, data=data)
        except requests.exceptions.ConnectionError as e:
            error = 'Unable to establish connection with Piper Server: {}'.format(e)
            LOGGER.error(msg=error)
            raise RuntimeError(error)
        if request.status_code != 200:
            raise RuntimeError('Invalid request ({0}) for {1}: {2}'.format(request.status_code, url, request.json()))

        # Serialize the data retrieved from the server:
        data = request.json()

        # Reset the query fields if applicable:
        if self.query_fields and self.query_fields_reset:
            self.query_fields = []

        # Return a single value if an attribute has been retrieved:
        if data and 'value' in data:
            return data['value']

        # Convert the retrieved server data into an entity/entities:
        model = eval(entity_type)
        if entity_id:
            return model(api=self, data=data)
        return [model(api=self, data=instance) for instance in data]

    def get_one(self, entity_type, **kwargs):
        """
        Get the first entity from a list of queried entities.
        :param entity_type: the (str) entity type;
        :param kwargs: any additional keyword arguments;
        :return: an entity if available, None otherwise;
        """
        result = self.get(entity_type=entity_type, **kwargs)
        if result:
            return result[0]
        return None

    def request(self, url, request_type='GET', data=None):
        """
        Make an arbitrary request to the Piper Server.
        :param url: the (str) URL to access;
        :param request_type: the type of the request - 'GET', 'POST', 'PUT' or 'DELETE';
        :param data: JSON data to include in the request;
        :return: a requests.get output;
        """
        if not data:
            data = self._gather_request_data()
        if self.server not in url:
            url = '{0}/{1}/'.format(self.server, url)
        if request_type.upper() == 'GET':
            return requests.get(url, auth=self.auth, data=data).json()
        elif request_type.upper() == 'POST':
            return requests.post(url, auth=self.auth, data=data).json()
        elif request_type.upper() == 'PUT':
            return requests.put(url, auth=self.auth, data=data).json()
        elif request_type.upper() == 'DELETE':
            return requests.delete(url, auth=self.auth, data=data).json()
        else:
            pass

    @staticmethod
    def _data_cleanup(data):
        """
        Check the values in a dictionary and convert them to entities if applicable.
        :param data: dictionary data;
        :return: dictionary data;
        """
        result = dict()
        for key, value in data.items():
            if issubclass(type(value), Entity):
                result[key] = json_to_string(value.data)
            elif type(value) == dict or type(value) == list:
                result[key] = json_to_string(value)
            else:
                result[key] = value
        return result

    def create(self, entity_type, **kwargs):
        """
        Create an entity instance of a certain type.
        :param entity_type: a (str) entity type;
        :param kwargs: any keyword arguments to be passed on to the server request;
        :return: a newly created entity;
        """
        data = self._data_cleanup(data=kwargs)
        data['created_by'] = self.artist.username
        url = '{0}/{1}/'.format(self.server, entity_type)
        LOGGER.debug(msg='Requesting (POST): %s' % url)
        request = requests.post(url, auth=self.auth, data=data)
        if request.status_code != 201:
            LOGGER.error(msg=request.text)
            raise RuntimeError(request.text)
        data = string_to_json(request.text)
        return eval(entity_type)(api=self, data=data)

    def update(self, entity):
        """
        Make an entity update server request.
        :param entity: an entity;
        :return: an updated entity;
        """
        data = self._data_cleanup(entity.data)
        data['updated_by'] = self.artist.username
        request = requests.put('{0}/{1}/{2}/'.format(self.server, entity.type, entity.id),
                               auth=self.auth,
                               data=data)
        data = request.json()
        if request.status_code == 400:
            LOGGER.error(msg=data)
            raise RuntimeError(data)
        return eval(entity.type)(api=self, data=data)

    def delete(self, entity):
        """
        Make an entity deletion server request.
        :param entity: an entity;
        :return: a delete request;
        """
        if type(entity) == dict:
            data = entity
        else:
            data = entity.data
        data['updated_by'] = self.artist.username
        return requests.delete('{0}/{1}/{2}/'.format(self.server, entity['type'], entity['id']),
                               auth=self.auth,
                               data=data)

    def get_current_artist(self, fields=None):
        """
        Get the entity for the current user.
        :return: an artist entity;
        """
        if not fields:
            fields = ['email', 'is_administrator', 'username']

        try:
            self._artist = self.get_one('User', filters={'username': self.user_name}, fields=fields)
        except IndexError:
            msg = 'User {} is not valid.'.format(self.user_name)
            LOGGER.error(msg=msg)
            raise RuntimeError(msg)
        return self._artist

    def get_active_tasks(self, dcc=None):
        """
        Get all active tasks for the current user and given DCC.
        :param dcc: a DCC entity;
        :return: a list of task entities;
        """
        if not dcc:
            return self.artist.active_tasks
        return self.get(entity_type='Task',
                        url='User/{0}/Software/{1}/tasks'.format(self.artist.id, dcc.id),
                        fields=['project', 'display_fields', 'shot', 'asset'])

    @property
    def artist(self):
        """
        Get the current artist.
        :return: an artist entity;
        """
        if not self._artist:
            self.get_current_artist()
        return self._artist

    @property
    def globals(self):
        """
        Get the studio globals config entity.
        :return: a globals entity;
        """
        if not self._globals:
            self.get_globals()
        return self._globals

    def get_globals(self, fields=['__all__']):
        """
        Get the GlobalsConfig entity from the Piper Server.
        :param fields: a list of fields to include in the query;
        :return: an GlobalsConfig entity;
        """
        self._globals = self.get(entity_type='GlobalsConfig', entity_id=1, fields=fields)
        return self._globals

    def get_config(self, entity):
        """
        Get a given entity's related config entity.
        :param entity: a target entity;
        :return: the given entity's config entity if it exists - None otherwise;
        """
        return self.get_one(entity_type='Config', filters={'parent': entity}, fields=['config_rules'])

    def get_inherited_config(self, entity):
        """
        Get a given entity's inherited config rules.
        :param entity: a target entity;
        :return: a config rules dictionary;
        """
        return requests.get('{0}/config/{1}/{2}/'.format(self.server, entity.type, entity.id), auth=self.auth).json()

    def get_top_configuration(self, entity, attribute):
        """
        Get the top level configuration of a certain attribute of a certain entity based on inherited configuration.
        :param entity: an entity;
        :param attribute: the (str) name of the attribute;
        :return: the value of the attribute if available;
        """
        inherited_config_rules = self.get_inherited_config(entity=entity)
        if inherited_config_rules:
            for key in reversed(inherited_config_rules.keys()):
                if inherited_config_rules[key] and attribute in inherited_config_rules[key]:
                    return inherited_config_rules[key][attribute]
        return None

    def configure(self, entity, config_rules, config=None):
        """
        Create a config entity in the database.
        :param entity: the entity, which the config entity refers to;
        :param config_rules: dictionary data to populate the config entity;
        :param config: config entity if available;
        :return: a created/updated config entity;
        """
        if not config:
            config = self.get_config(entity=entity)
        if not config:
            data = dict(name=entity.name,
                        long_name=entity.long_name,
                        _parent_model=entity.type,
                        _parent_id=entity.id,
                        config_rules=config_rules)

            config = self.create(entity_type='Config', **data)
        else:
            config['config_rules'] = config_rules
            self.update(config)
        LOGGER.info('Successfully configured %s %s.' % (entity['type'].lower(), entity['long_name']))
        return config

    def get_dependency(self, entity):
        """
        Get a given entity's dependency entity.
        :param entity: a target entity;
        :return: the given entity's dependency entity if it exists - None otherwise;
        """
        dependency = self.get_one(entity_type='Dependency',
                                  filters={'dependant_model': entity.type, 'dependant_id': entity.id},
                                  fields=['dependant_model', 'dependant_id', 'dependencies'])
        return dependency

    def send_ticket(self, message):
        """
        Attempt to send a ticket to the configured ticketing email address.
        :param message: the ticket's (str) message;
        :return: True and JSON response if successful, False and (str) message otherwise;
        """
        from_email = self.artist.email
        if not from_email:
            return False, 'No e-mail address found for user {}.'.format(self.artist.username)
        to_address = self.globals.ticket_email
        if not to_address:
            return False, 'No ticket e-mail address configured in the global settings.'
        return True, self.notify(subject='Piper Client Error Ticket', message=message, from_email=from_email,
                                 recipients=[to_address], verbose_email=True)

    @property
    def system(self):
        """
        Get the current system entity.
        :return: a system entity;
        """
        if not self._system:
            fields = ['publish_root', 'work_root', 'software_root', 'modules_root']
            if 'PIPER_SYSTEM_ID' in os.environ:
                self._system = self.get(entity_type='System',
                                        entity_id=os.environ['PIPER_SYSTEM_ID'],
                                        fields=fields)
            else:
                self._system = self.get_one(entity_type='System',
                                            filters={'platform': sys.platform},
                                            fields=fields)
        return self._system

    def publish(self, entity):
        """
        Make a publishing (post) request to the Piper Server for a given entity.
        :param entity: the entity to publish;
        :return: JSON request response;
        """

        try:
            request = requests.post('{0}/publish/{1}/{2}/'.format(self.server, entity.type, entity.id),
                                    auth=self.auth, data={'updated_by': self.context_user_name})
        except requests.exceptions.ConnectionError as e:
            error = 'Unable to establish connection with Piper Server: {}'.format(e)
            LOGGER.error(msg=error)
            raise RuntimeError(error)
        return request.json()

    def publish_from_id(self, entity_type, entity_id):
        """
        Make a publishing (post) request to the Piper Server for a given entity type and ID.
        :param entity_type: the (str) type of entity to publish;
        :param entity_id: a specific (int) ID of the target entity to publish;
        :return: JSON request response;
        """
        entity = self.get(entity_type=entity_type, entity_id=entity_id)
        return self.publish(entity=entity)

    def notify(self, subject, message, recipients, from_email=None, verbose_email=False):
        """
        Send an e-mail notification if Piper Server is configured to send e-mail.
        :param subject: the (str) subject of the e-mail;
        :param message: the (str) e-mail message;
        :param recipients: a list of user names;
        :param from_email: the (str) author's e-mail address;
        :param verbose_email: (bool) whether or not to use full e-mail addresses rather than usernames;
        :return: JSON request response;
        """
        if not from_email and self.artist:
            from_email = self.artist.email
        data = {'subject': subject, 'message': message, 'recipients': recipients, 'verbose_email': verbose_email,
                'from_email': from_email}
        return requests.post('{}/notify/email/'.format(self.server), auth=self.auth, data=data).json()

    def absolute_url(self, url):
        """
        Get the absolute URL, including server address, for the given URL.
        :param url: a (str) relative link;
        :return: a (str) URL;
        """
        server = os.path.dirname(self.server)
        if url.startswith(server):
            return url
        return '{0}/{1}'.format(server, self._url_sanity_check(url))

    def sync_to_sg(self, entity):
        """
        Attempt to sync a given entity to Shotgun.
        :param entity: the entity to sync;
        :return: JSON request response;
        """
        try:
            request = requests.post('{0}/sync/{1}/{2}/'.format(self.server, entity.type, entity.id), auth=self.auth)
        except requests.exceptions.ConnectionError as e:
            error = 'Unable to establish connection with Piper Server: {}'.format(e)
            LOGGER.error(msg=error)
            raise RuntimeError(error)
        return request.json()

    def open_current_entity_page(self):
        """
        Open the web page of the current version, resource or task stored in the environment.
        :return: None;
        """
        if environment_check('PIPER_VERSION_ID'):
            entity = self.get(entity_type='Version', entity_id=os.environ['PIPER_VERSION_ID'], fields=['full_url'])
        elif environment_check('PIPER_RESOURCE_ID'):
            entity = self.get(entity_type='Resource', entity_id=os.environ['PIPER_RESOURCE_ID'], fields=['full_url'])
        elif environment_check('PIPER_TASK_ID'):
            entity = self.get(entity_type='Task', entity_id=os.environ['PIPER_TASK_ID'], fields=['full_url'])
        else:
            return LOGGER.error(msg='No version, resource or task is currently being worked on.')

        self.open_entity_page(entity=entity)

    @staticmethod
    def open_entity_page(entity):
        """
        Open the web page for the given entity.
        :param entity: an entity instance;
        :return: None;
        """
        LOGGER.info('Opening URL: %s' % entity.url)
        webbrowser.open(entity.url)
