# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import logging
import getpass
import datetime


def get_logger_file_path():
    """
    Get the full path to the user's current log file.
    :return: a full file path (string);
    """
    now = datetime.datetime.now()
    file_name = '{0}{1}{2}.log'.format(now.year, now.month, now.day)
    logs_dir = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'logs')),
                            getpass.getuser())
    # If the logs directory doesn't exist, create it:
    if not os.path.exists(logs_dir):
        os.makedirs(logs_dir)
    return os.path.join(logs_dir, file_name)


def get_logger(name):
    """
    Create a logger object to log the activity of some module.
    :param name: the name given to the logger object;
    :return: a logger object;
    """
    logger = logging.getLogger(name)
    if not len(logger.handlers):
        logger_file_path = get_logger_file_path()
        # Format the logger's output:
        log_formatter = logging.Formatter(
            "%(asctime)s [%(levelname)-5.5s] [%(name)s] [%(module)s] [%(funcName)s] > %(message)s")

        # Create a file handler for the logger's output:
        file_handler = logging.FileHandler(logger_file_path)
        file_handler.setFormatter(log_formatter)
        logger.addHandler(file_handler)

        # Create a console handler for the logger's output:
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(log_formatter)
        logger.addHandler(console_handler)
        logger.setLevel(logging.DEBUG)
    logger.info('Starting...')
    return logger
