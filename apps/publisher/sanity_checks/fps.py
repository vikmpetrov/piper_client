# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from . import SanityCheck as SanityCheckBase


class SanityCheck(SanityCheckBase):

    name = 'FPS'
    description = 'Correct frames per second sanity check.'
    required = False
    checked = True
    priority = 99

    def __init__(self, **kwargs):
        super(SanityCheck, self).__init__(**kwargs)
        self.fps = None

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        if self.resource.task.shot:
            return True
        return False

    def run(self):
        """
        The main function executed by the sanity check.
        :return: True/False;
        """
        self.fps = self.piper_api.get_top_configuration(self.resource, 'fps')
        if self.fps:
            current_fps = self.dcc_api.get_fps()
            if self.fps != current_fps:
                self.logger.error('Scene FPS {0} does not match the configured FPS of {1}.'.format(current_fps,
                                                                                                   self.fps))
                return False
        else:
            self.logger.warn('No FPS configuration found.')
        return True

    def fix(self):
        """
        A method that attempts a fix should the sanity check fail (return False) on run.
        :return: True/False;
        """
        self.dcc_api.set_fps(self.fps)
        self.logger.info('Set FPS to {}.'.format(self.fps))
        return True
