# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
from glob import glob
from shutil import copyfile
from piper_client.gui import MainWindow
from piper_client.gui.qt import QtGui, QtWidgets
from piper_client.gui.utils import warning_message, error_message, info_message, set_combo_box_text, load_qt_ui
from piper_client.gui.widgets.file_selector import FileSelector
from piper_client.gui.widgets.publishing_processor import PublishingProcessor
from piper_client.core.api import PiperClientApi
from piper_client.core.dcc import load_dcc_api
from piper_client.core.utils import environment_check, piper_except_hook
from .sanity_checks import get_sanity_checks
from .post_processes import get_post_processes


class Publisher(MainWindow):
    """
    An app for publishing data to the database.
    """
    def __init__(self, dcc_api=None, resource=None, ui=True, parent=None):
        MainWindow.__init__(self, parent, 'Piper Publisher', 'publisher.ui')
        self.api = PiperClientApi()
        dcc = os.environ['PIPER_DCC']
        self.dcc_api = dcc_api
        if not self.dcc_api:
            dcc_api = load_dcc_api(dcc.lower())
            self.dcc_api = dcc_api()
        self.logger.info('Starting in %s...' % dcc)
        self.file_selector = None
        self.selected_resource = None
        self.proceed = False
        self.ui = ui  # whether to run Publisher standalone or with a UI;

        if not environment_check('PIPER_TASK_ID'):
            self.logger.error('No task currently being worked on, aborting.')
            error_message(parent=self, msg='Task not found!' +
                          '\nPlease select a task from the Piper Scene Builder.')
            self.close()
        else:
            self.current_task = self.api.get(entity_type='Task', entity_id=int(os.environ['PIPER_TASK_ID']))

            if resource:
                self.selected_resource = resource
            else:
                if not environment_check('PIPER_RESOURCE_ID'):
                    self.logger.error('Could not find valid resource, aborting.')
                    error_message(parent=self, msg='Valid resource not found!' +
                                  '\nPlease open a resource version or snapshot from the Piper Scene Builder.')
                else:
                    self.selected_resource = self.api.get(entity_type='Resource',
                                                          entity_id=int(os.environ['PIPER_RESOURCE_ID']))

            if self.ui and self.selected_resource:
                self.mainLabel.setText('Publish {0}'.format(self.selected_resource.long_name))
                self._setup_data()
                self._connect_widget_cmds()
                self.show()

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None
        """
        self.file_selector = FileSelector(parent=self)
        current_file = self.dcc_api.get_current_file_name()
        if not current_file:
            current_file = 'current work file'
        self.sourceFileLineEdit.setText(current_file)
        self.filesLayout.addWidget(self.file_selector)
        self.file_selector.selectFilesGrp.setTitle('Additional file(s):')
        self.projectLabel.setText('Project: {}'.format(self.selected_resource.project.long_name))
        self.entityLabel.setText('Entity: {0}_{1}'.format(self.selected_resource.task.link.sequence.long_name,
                                                          self.selected_resource.task.link.long_name))
        self.resourceLabel.setText('Resource: {}'.format(self.selected_resource.long_name))

    def _get_arg_dict(self):
        """
        Gather publishing data from the UI and calls the relevant publishing functions
        based on what is to be published.
        :return: None;
        """
        description = self.descriptionTextEdit.toPlainText()
        for_dailies = self.forDailiesCheckBox.isChecked()

        arg_dict = {'description': description,
                    'is_work_file': True,
                    'for_dailies': for_dailies,
                    'selection_only': self.selectionCheckBox.isChecked(),
                    'file_format': self.dcc_api.get_current_file_format()}
        return arg_dict

    def publish(self, arg_dict=None):
        """
        Run the appropriate publishing method with relevant arguments.
        :param arg_dict: publishing arguments dictionary if applicable;
        :return: True if successful, otherwise - False;
        """
        if not arg_dict:
            arg_dict = self._get_arg_dict()

        if not arg_dict['description']:
            warning_message(parent=self, msg='Please provide a description for what you are publishing.')
            return None

        sanity_checks = []
        for sanity_check_module in get_sanity_checks():
            sanity_check = sanity_check_module(dcc_api=self.dcc_api, resource=self.selected_resource)
            if sanity_check.applicable:
                sanity_checks.append(sanity_check)
        sanity_checks.sort(key=lambda x: x.priority, reverse=True)

        publishing_processor = PublishingProcessor(parent=self,
                                                   process_type='sanity_check',
                                                   processes=sanity_checks)
        publishing_processor.exec_()

        if not self.proceed:
            error_message(parent=self, msg='Did not pass all sanity checks, publishing aborted.')
            return False

        version = self.publish_version(arg_dict=arg_dict)
        os.environ['PIPER_VERSION_ID'] = str(version.id)

        if self.ui:
            notify = self.notifyLineEdit.text()
            if notify:
                recipients = [x for x in ' '.join(notify.split(',')).split() if x]
                if recipients:
                    subject = '{0} | Published version {1}'.format(version.project.long_name, version.long_name),
                    message = 'Version {0} has been published: \nURL: {1}\nPath: {2}'.format(version.long_name,
                                                                                             version.full_url,
                                                                                             version.publish_path)
                    result = self.api.notify(subject=subject,
                                             message=message,
                                             recipients=','.join(recipients))
                    if result['warnings']:
                        warning_message(parent=self, msg='\n'.join(result['warnings']))

        self.proceed = False

        post_processes = []
        for process_module in get_post_processes():
            process = process_module(dcc_api=self.dcc_api, version=version, parent=self)
            if process.applicable:
                post_processes.append(process)
        post_processes.sort(key=lambda x: x.priority, reverse=True)

        publishing_processor = PublishingProcessor(parent=self,
                                                   process_type='post_process',
                                                   processes=post_processes)
        publishing_processor.exec_()

        if not self.proceed:
            warning_message(parent=self, msg='Did not run any post-publishing processes...\n' +
                                             'This may cause errors further down the pipeline.')
        else:
            info_message(parent=self, msg='Publishing done!')

        return True

    def publish_version(self, arg_dict):
        """
        Publish a new version and related publish files.
        :param arg_dict: publishing arguments dictionary;
        :return: None;
        """
        # Create a version of the current resource:
        version = self.api.create(entity_type='Version',
                                  resource=self.selected_resource,
                                  artist=self.api.get_current_artist(),
                                  for_dailies=arg_dict['for_dailies'],
                                  description=arg_dict['description'])

        file_entity = self.api.create(entity_type='File',
                                      version=version,
                                      artist=version.artist,
                                      is_work_file=arg_dict['is_work_file'],
                                      format=arg_dict['file_format'])

        # Save the file to its VFX work path:
        work_path = file_entity.work_path

        # Create parent work directory if it doesn't exist:
        parent_dir = os.path.dirname(work_path)
        if not os.path.isdir(parent_dir):
            os.makedirs(parent_dir)

        self.dcc_api.save_work_file(file_entity=file_entity, selection_only=arg_dict['selection_only'])
        published = self.api.publish(entity=file_entity)

        if published['status']:
            self.logger.info('Created %s resource %s version %s.' % (self.selected_resource.type,
                                                                     self.selected_resource.long_name,
                                                                     version.name))
            info_message(parent=self, msg='Resource version successfully created!')
        else:
            error_message(parent=self, msg='Resource version created, but there was an error saving out its file...')

        self.publish_auxiliary_files(version=version)
        return version

    def publish_auxiliary_files(self, version):
        """
        Publish files from the file selector.
        :param version: the parent version entity;
        :return: None;
        """
        if not self.file_selector:
            return

        for selected_file in self.file_selector.file_drop.files:
            # File sequence check:
            is_sequence = False
            if '.####.' in selected_file:
                is_sequence = True

            file_entity = self.api.create(entity_type='File',
                                          is_sequence=is_sequence,
                                          version=version,
                                          artist=version.artist,
                                          format=selected_file.split('.')[-1])

            # Create parent directory if it doesn't exist:
            parent_dir = os.path.dirname(file_entity.work_path)
            if not os.path.isdir(parent_dir):
                os.makedirs(parent_dir)

            if is_sequence:
                # Get the appropriate start frame from the resource configuration or from the show configuration:
                configured_start_frame = version.config.inherited_get('start_frame')
                if not configured_start_frame:
                    configured_start_frame = version.globals.start_frame
                set_file_index = configured_start_frame

                for file in sorted(glob(r'{}'.format(selected_file))):
                    if set_file_index:
                        file_index = set_file_index
                        set_file_index += 1
                    else:
                        file_index = file.split('.')[-2]
                    copyfile(file, file_entity.work_path.replace('.####.', '.{}.'.format(file_index)))
            else:
                copyfile(selected_file, file_entity.work_path)

            published = self.api.publish(entity=file_entity)
            if published['status']:
                self.logger.info(published['message'])
            else:
                self.logger.error(published['message'])
                
    def publish_cmd(self):
        """
        Run the "publish" method and close.
        :return: None;
        """
        if self.publish():
            self.close()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.publishBtn.clicked.connect(self.publish_cmd)
        self.cancelBtn.clicked.connect(self.close)


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QtWidgets.QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show(parent=None):
    """
    Show the UI.
    :param parent: parent Qt window;
    :return: None;
    """
    sys.excepthook = piper_except_hook
    publisher = Publisher(parent=parent)


if __name__ == '__main__':
    run()
