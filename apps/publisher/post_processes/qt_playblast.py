# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
from . import PostProcess as PostProcessBase


class PostProcess(PostProcessBase):

    name = 'QT playblast'
    description = 'Generate a playblast preview for this version.'
    required = False
    checked = False
    priority = 98

    def __init__(self, **kwargs):
        super(PostProcess, self).__init__(**kwargs)

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        if self.dcc_api.name == 'maya':
            return True
        return False

    def run(self):
        """
        The main function executed by the process.
        :return: True/False;
        """

        # Create work file:
        file_entity = self.piper_api.create(entity_type='File',
                                            is_preview=True,
                                            version=self.version,
                                            artist=self.version.artist,
                                            format='mov')

        # Create parent directory if it doesn't exist:
        parent_dir = os.path.dirname(file_entity.work_path)
        if not os.path.isdir(parent_dir):
            os.makedirs(parent_dir)

        # Playblast and save MOV:
        playblast = self.dcc_api.src.playblast(filename=file_entity.work_path, format='movie', sequenceTime=False,
                                               clearCache=True, viewer=True, showOrnaments=True, fp=4, percent=100,
                                               compression='H.264', quality=70, forceOverwrite=True)

        # Publish work file:
        published = self.piper_api.publish(entity=file_entity)
        if published['status']:
            self.logger.info(published['message'])
        else:
            self.logger.error(published['message'])

        return True
