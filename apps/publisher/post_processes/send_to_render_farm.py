# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import piper_client.apps.render_farmer as piper_render_farmer
from . import PostProcess as PostProcessBase


class PostProcess(PostProcessBase):

    name = 'Send to render farm'
    description = 'Launch the Piper Render Farmer to submit this version as a render farm job.'
    required = False
    checked = False
    priority = 97

    def __init__(self, **kwargs):
        super(PostProcess, self).__init__(**kwargs)

    @property
    def applicable(self):
        """
        A property to determine if the process is applicable for the given step;
        :return: True/False;
        """
        return True

    def run(self):
        """
        The main function executed by the process.
        :return: True/False;
        """
        piper_render_farmer.show(parent=self.parent)
        return True
