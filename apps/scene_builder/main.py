# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import sys
import os
from piper_client.gui import MainWindow
from piper_client.gui.qt import QtGui, QtCore, QtWidgets
from piper_client.gui.utils import info_message, error_message, confirm_dialog, set_combo_box_text, load_qt_ui
from piper_client.gui.widgets.create_resource import CreateResource
from piper_client.core.api import PiperClientApi
from piper_client.core.dcc import load_dcc_api
from piper_client.core.utils import environment_check, browse_path, piper_except_hook
from piper_client.gui.widgets.log_work import LogWork

TASKS_TABLE_HEADER = ['Project', 'Entity', 'Name', 'Description', 'Step', 'Status', 'Start Date', 'Due Date']
ASSEMBLY_TREE_HEADER = ['Entity', 'Resource', 'Version', 'Description', 'Latest', 'File', 'Action', 'Action', 'Action']


class TaskThread(QtCore.QThread):
    """
    A thread object for populating the Scene Builder task table rows.
    """

    task_selected = QtCore.Signal(object)

    def __init__(self, parent, index, task):
        super(TaskThread, self).__init__(parent)
        self.index = index
        self.task = task
        self.parent = parent

    def run(self):
        self.parent.data['tasks'][self.index] = self.task
        self.add_task_to_table_row()
        if self.parent.current_task and self.task.id == self.parent.current_task.id:
            self.parent.tasksTableWidget.selectRow(self.index)
            self.task_selected.emit(True)

    def add_task_to_table_row(self):
        """
        Represent a given task as a table row in the tasks table widget.
        :return: None;
        """
        self.parent.tasksTableWidget.setItem(self.index, 0, QtWidgets.QTableWidgetItem(self.task.project.long_name))
        self.parent.tasksTableWidget.setItem(self.index, 1, QtWidgets.QTableWidgetItem('{0} {1}'.format(
            self.task.link.type,
            self.task.link.long_name)))
        self.parent.tasksTableWidget.setItem(self.index, 2, QtWidgets.QTableWidgetItem(self.task.long_name))
        self.parent.tasksTableWidget.setItem(self.index, 3, QtWidgets.QTableWidgetItem(
            self.task.display_fields['Description']['value']))
        self.parent.tasksTableWidget.setItem(self.index, 4, QtWidgets.QTableWidgetItem(
            self.task.display_fields['Step']['value']))
        self.parent.tasksTableWidget.setItem(self.index, 5, QtWidgets.QTableWidgetItem(
            self.task.display_fields['Status']['value']))
        self.parent.tasksTableWidget.setItem(self.index, 6, QtWidgets.QTableWidgetItem(
            self.task.display_fields['Start date']['value']))
        self.parent.tasksTableWidget.setItem(self.index, 7, QtWidgets.QTableWidgetItem(
            self.task.display_fields['End date']['value']))


class PopulateThread(QtCore.QThread):
    """
    A thread object for populating the Scene Builder assembly tree.
    """

    def __init__(self, parent, referenced_file_ids, file_id, parent_item):
        super(PopulateThread, self).__init__(parent)
        self.parent = parent
        self.referenced_file_ids = referenced_file_ids
        self.entity = self.parent.api.get(entity_type='File', entity_id=file_id, fields=['version', 'format'])
        self.parent_item = parent_item

    def run(self):
        item = self.parent.append_to_assembly_tree(parent_item=self.parent_item, entity=self.entity)
        self.parent.build_assembly_tree(referenced_file_ids=self.referenced_file_ids[self.entity.id], parent_item=item)


class SceneBuilder(MainWindow):
    """
    An object to manage scene assembly in a given DCC.
    """
    def __init__(self, parent=None):
        MainWindow.__init__(self, parent, 'Piper Scene Builder', 'scene_builder.ui')
        self.api = PiperClientApi()
        self.dcc = self.api.get(entity_type='Software',
                                entity_id=int(os.environ['PIPER_SOFTWARE_ID']),
                                fields=['category', 'file_extensions'])
        self.logger.info('Starting in %s...' % self.dcc.long_name)
        dcc_api = load_dcc_api(self.dcc.name)
        self.dcc_api = dcc_api(software=self.dcc)
        self.data = {
            'asset_types': {},
            'assets': {},
            'asset_resources': {},
            'shot_resources': {},
            'workspace_resources': {},
            'snapshots': {},
            'tasks': {},
            'assembly_files': {},
            'shot_versions': {},
            'assembly_items': {}
        }  # To store label-to-entity references.
        self.artist = self.api.get_current_artist()
        self.current_task = None
        self.resource_created = False
        self.item_entity_dict = dict()
        if not self.artist:
            error = 'The current user could not be found in the Piper database.'
            error_message(parent=self, msg=error)
            raise(Exception(error))
        else:
            self._setup_data()
            self._connect_widget_cmds()

    def _setup_data(self):
        """
        Populate various UI elements with data.
        :return: None;
        """

        tasks = self.api.get_active_tasks(dcc=self.dcc)
        self.tasksTableWidget.clear()
        self.tasksTableWidget.setColumnCount(len(TASKS_TABLE_HEADER))
        self.tasksTableWidget.setHorizontalHeaderLabels(TASKS_TABLE_HEADER)
        self.tasksTableWidget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.current_task = self.get_context_task()

        self.tasksTableWidget.setRowCount(len(tasks))
        for i, task in enumerate(tasks):
            task_thread = TaskThread(parent=self, index=i, task=task)
            task_thread.task_selected.connect(self.re_populate_all)
            task_thread.start()

        self.populate_asset_types()

        self.assemblyTreeWidget.setHeaderItem(QtWidgets.QTreeWidgetItem(ASSEMBLY_TREE_HEADER))
        self.refresh_tree_data()

    def refresh_tree_data(self):
        """
        Setup the asset tree data.
        :return: None;
        """
        self.assemblyTreeWidget.clear()
        self.build_assembly_tree(referenced_file_ids=self.dcc_api.find_entity_nodes(),
                                 parent_item=self.assemblyTreeWidget)
        # Show all of the fields in the tree:
        self.assemblyTreeWidget.expandAll()

    def build_assembly_tree(self, referenced_file_ids, parent_item):
        """
        Represent the database as items in the UI's data tree.
        :param referenced_file_ids: a hirerarchical dictionary of IDs of version files present in the scene;
        :param parent_item: the parent of the generated QTreeWidgetItem;
        :return: None;
        """
        for file_id in referenced_file_ids:
            entity = self.api.get(entity_type='File', entity_id=file_id, fields=['version', 'format'])
            item = self.append_to_assembly_tree(parent_item=parent_item, entity=entity)
            self.build_assembly_tree(referenced_file_ids=referenced_file_ids[entity.id], parent_item=item)

    def append_to_assembly_tree(self, parent_item, entity):
        """
        Add a data row to the data tree and store its main item under a unique ID for future reference.
        :param parent_item: the parent item of the item to be created;
        :param entity: the entity to be represented;
        :return: a newly created data tree item;
        """

        item = QtWidgets.QTreeWidgetItem(parent_item)

        version_combo_box = QtWidgets.QComboBox(self.assemblyTreeWidget)
        all_versions = {}
        for version in sorted(entity.version.resource.versions, key=lambda x: x.long_name, reverse=True):
            version_combo_box.addItem(version.long_name)
            all_versions[version.long_name] = version
        set_combo_box_text(combo_box=version_combo_box, text=entity.version.long_name)
        is_latest_label = QtWidgets.QLabel(self.assemblyTreeWidget)
        is_latest_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        file_label = QtWidgets.QLabel(entity.format, self.assemblyTreeWidget)
        file_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        link_label = QtWidgets.QLabel('{0} {1}'.format(
                        entity.version.resource.parent.link.type,
                        entity.version.resource.parent.link.long_name),
                        self.assemblyTreeWidget)
        link_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        description_label = QtWidgets.QLabel('{}'.format(entity.version.description), self.assemblyTreeWidget)
        description_label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        remove_btn = QtWidgets.QPushButton('Remove', self.assemblyTreeWidget)
        remove_btn.setProperty('btn_type', 'danger')
        open_page_btn = QtWidgets.QPushButton('Open page', self.assemblyTreeWidget)
        find_btn = QtWidgets.QPushButton('Find', self.assemblyTreeWidget)

        self.assemblyTreeWidget.setItemWidget(item, 0, link_label)
        self.assemblyTreeWidget.setItemWidget(item, 1, QtWidgets.QLabel(entity.version.resource.long_name,
                                                                        self.assemblyTreeWidget))

        self.assemblyTreeWidget.setItemWidget(item, 2, version_combo_box)
        self.assemblyTreeWidget.setItemWidget(item, 3, description_label)
        self.assemblyTreeWidget.setItemWidget(item, 4, is_latest_label)
        self.assemblyTreeWidget.setItemWidget(item, 5, file_label)
        self.assemblyTreeWidget.setItemWidget(item, 6, remove_btn)
        self.assemblyTreeWidget.setItemWidget(item, 7, find_btn)
        self.assemblyTreeWidget.setItemWidget(item, 8, open_page_btn)

        self._assembly_item_version_check(item=item, label=is_latest_label, version=entity.version)

        item_id = self.assemblyTreeWidget.indexFromItem(item, 0).internalId()
        self.data['assembly_files'][entity.id] = {
            'item': item,
            'entity': entity,
            'all_versions': all_versions
        }

        self.data['assembly_items'][item_id] = {'entity': entity, 'item': item}

        remove_btn.clicked.connect(lambda: self.remove_assembly_tree_item(item_id=item_id))
        find_btn.clicked.connect(lambda: self._entity_path_check(entity=entity.version))
        open_page_btn.clicked.connect(lambda: self.api.open_entity_page(entity=entity))
        version_combo_box.currentIndexChanged.connect(lambda: self.update_assembly_item_version(item_id=item_id))
        item.setExpanded(True)
        return item

    def remove_assembly_tree_item(self, item_id):
        """
        Remove an item from the assembly tree as well as its corresponding reference file in the scene.
        :param item_id: the target item's ID;
        :return: None;
        """
        item = self.data['assembly_items'][item_id]['item']
        entity = self.data['assembly_items'][item_id]['entity']
        if confirm_dialog(parent=self, question='Are you sure you want to remove this version?'):
            invisible_root = self.assemblyTreeWidget.invisibleRootItem()
            (item.parent() or invisible_root).removeChild(item)
            self.dcc_api.remove_published_file(entity)
            self.data['assembly_files'].pop(entity.id, None)

    def update_assembly_item_version(self, item_id):
        """
        Update an version item's version and replace its corresponding reference file in the scene.
        :param item_id: the (str) unique ID of the target item;
        :return: an info message if successful, otherwise - an error message;
        """
        item = self.data['assembly_items'][item_id]['item']
        version_label = self.assemblyTreeWidget.itemWidget(item, 4)
        version_combo_box = self.assemblyTreeWidget.itemWidget(item, 2)
        description = self.assemblyTreeWidget.itemWidget(item, 3)
        open_page_btn = self.assemblyTreeWidget.itemWidget(item, 8)

        # Get the relevant data:
        resource = self.data['assembly_items'][item_id]['entity'].version.resource
        previous_file = self.data['assembly_items'][item_id]['entity']
        selected_version = self.data['assembly_files'][previous_file.id]['all_versions'][
            version_combo_box.currentText()]

        # Replace the previous version's referenced publish file in the scene with the new one:
        new_version_file = self.api.get_one(entity_type='File',
                                            filters={'version': selected_version, 'format': previous_file.format},
                                            fields=['format'])

        if not new_version_file:
            return error_message(parent=self,
                                 msg='Resource {0} version {1} does not have files matching the {2} format.'.format(
                                    resource.long_name, selected_version.long_name, previous_file.format))
        if not self.dcc_api.replace_published_file(previous_file_entity=previous_file,
                                                   new_file_entity=new_version_file):
            version_combo_box.blockSignals(True)
            set_combo_box_text(combo_box=version_combo_box, text=previous_file.version.long_name)
            version_combo_box.blockSignals(False)
            open_page_btn.clicked.disconnect()
            open_page_btn.clicked.connect(lambda: self.api.open_entity_page(entity=new_version_file))
            return error_message(parent=self, msg='Resource {0} file {1} replacement to {2} unsuccessful!'.format(
                resource.long_name, previous_file.long_name, selected_version.long_name))
        else:
            self.data['assembly_items'][item_id]['entity'] = new_version_file

        referenced_file_ids = self.dcc_api.find_entity_nodes(root=new_version_file)

        # Remove all item children and rebuild its assembly tree:
        item.takeChildren()

        # Build the new version's assembly tree branch with any child references:
        self.build_assembly_tree(referenced_file_ids=referenced_file_ids, parent_item=item)
        self.data['assembly_files'][new_version_file.id] = self.data['assembly_files'].pop(previous_file.id, None)
        self._assembly_item_version_check(item=item, label=version_label, version=selected_version)
        description.setText(selected_version.description)

        return info_message(parent=self, msg='Replaced {0} version {1} file with matching {2} file!'.format(
                resource.long_name, previous_file.version.long_name, selected_version.long_name))

    def _assembly_item_version_check(self, item, label, version):
        """
        Check if the given version is the latest and reflect that in its item.
        :param item: the item of the version's "latest" cell;
        :param label: the label of the version's "latest" cell;
        :param version: a version entity;
        :return: None;
        """
        label_type = 'error'
        if version.is_latest:
            label_type = 'success'
        widget = self.assemblyTreeWidget.itemWidget(item, 4)
        widget.setProperty('label_type', label_type)
        widget.style().unpolish(widget)
        widget.style().polish(widget)
        label.setText(str(version.is_latest))

    def set_task_environment(self, index):
        """
        Set the current task environment based on the currently selected task table cell.
        :param index: the (int) index of the currently selected task table cell;
        :return: None;
        """
        self.current_task = self.data['tasks'][index]
        os.environ['PIPER_TASK_ID'] = str(self.current_task.id)
        os.environ['PIPER_RESOURCE_ID'] = ''
        os.environ['PIPER_VERSION_ID'] = ''
        self.logger.info('Set Piper context from task ID: %s' % self.current_task.id)
        self.populate_assets()
        self.populate_workspace_resources()
        self.populate_shot_resources()

    def on_cell_double_clicked(self, index):
        """
        Open the double-clicked task's Piper Server web page.
        :param index: the (int) index of the currently selected task table cell;
        :return: None;
        """
        self.current_task = self.data['tasks'][index]
        self.api.open_entity_page(entity=self.current_task)

    def re_populate_all(self):
        """
        Re-populate various UI elements with data.
        :return: None;
        """
        self.populate_workspace_resources()
        self.populate_workspace_versions()
        self.populate_snapshots()
        self.populate_assets()
        self.populate_asset_resources()
        self.populate_asset_resource_versions()
        self.populate_shot_resources()
        self.populate_shot_resource_versions()

    def get_context_task(self):
        """
        Get the environment context task.
        :return: a task entity if found, otherwise - None;
        """
        if environment_check('PIPER_TASK_ID'):
            self.api.query_fields = ['asset', 'shot', 'project']
            return self.api.get(entity_type='Task', entity_id=int(os.environ['PIPER_TASK_ID']))
        return None

    def populate_asset_types(self):
        """
        Populate with asset types.
        :return:
        """
        self.assetTypeComboBox.clear()
        self.assetTypeComboBox.addItem('')
        asset_types = self.api.get(entity_type='AssetType')
        for asset_type in sorted(asset_types, key=lambda x: x.long_name):
            self.assetTypeComboBox.addItem(asset_type.long_name)
            self.data['asset_types'][asset_type.long_name] = asset_type

    def populate_assets(self):
        """
        Populate with assets of the currently selected asset type.
        :return: None;
        """
        self.assetComboBox.clear()
        self.data['assets'] = {}
        self.assetComboBox.addItem('')
        asset_type = self.assetTypeComboBox.currentText()
        if self.current_task and asset_type:
            asset_type = self.data['asset_types'][asset_type]
            assets = self.api.get(entity_type='Asset',
                                  filters={'asset_type': asset_type, 'project': self.current_task.project})
            for asset in sorted(assets, key=lambda x: x.long_name):
                self.data['assets'][asset.long_name] = asset
                self.assetComboBox.addItem(asset.long_name)

    def populate_shot_resources(self):
        """
        Populate with shot resources of the currently selected task's shot if applicable.
        :return: None;
        """
        self.shotResourceComboBox.clear()
        self.data['shot_resources'] = {}
        self.shotResourceComboBox.addItem('')
        if self.current_task and self.current_task.shot:
            for resource in sorted(self.current_task.shot.resources, key=lambda x: x.long_name):
                self.data['shot_resources'][resource.long_name] = {'entity': resource, 'versions': {}}
                self.shotResourceComboBox.addItem(resource.long_name)

    def populate_asset_resources(self):
        """
        Add resources to the assembly asset UI elements.
        :return: None;
        """
        self.assetResourceComboBox.clear()
        self.data['asset_resources'] = {}
        self.assetResourceComboBox.addItem('')
        selected_asset = self.assetComboBox.currentText()
        if selected_asset:
            # Get the selected asset's resources:
            asset_entity = self.data['assets'][selected_asset]
            for resource_entity in sorted(asset_entity.resources, key=lambda x: x.long_name):
                # Store the resource in the context data dictionary for future reference:
                self.data['asset_resources'][resource_entity.long_name] = {'entity': resource_entity,
                                                                            'versions': {}}
                self.assetResourceComboBox.addItem(resource_entity.long_name)

    def populate_shot_resource_versions(self):
        """
        Add resource versions to the assembly shot UI elements.
        :return: None;
        """
        self.shotVersionComboBox.clear()
        selected_resource = self.shotResourceComboBox.currentText()
        self.shotVersionComboBox.addItem('')
        if selected_resource:
            # Get the selected resource's versions:
            resource_entity = self.data['shot_resources'][selected_resource]['entity']
            for version_entity in sorted(resource_entity.versions, key=lambda x: x.long_name, reverse=True):
                # Store the version in the context data dictionary for future reference:
                self.data['shot_resources'][resource_entity.long_name]['versions'][
                    version_entity.long_name] = version_entity
                self.shotVersionComboBox.addItem(version_entity.long_name)

    def populate_asset_resource_versions(self):
        """
        Add resource versions to the assembly asset UI elements.
        :return: None;
        """
        self.assetVersionComboBox.clear()
        self.assetVersionComboBox.addItem('')
        selected_resource = self.assetResourceComboBox.currentText()
        if selected_resource:
            # Get the selected resource's versions:
            resource_entity = self.data['asset_resources'][selected_resource]['entity']
            self.api.query_fields = ['files', 'format']
            for version_entity in sorted(resource_entity.versions, key=lambda x: x.long_name, reverse=True):
                # Store the version in the context data dictionary for future reference:
                self.data['asset_resources'][resource_entity.long_name]['versions'][version_entity.long_name] = \
                                                                                        version_entity
                self.assetVersionComboBox.addItem(version_entity.long_name)

    def populate_asset_version_files(self):
        """
        Add files to the assembly asset file UI elements.
        :return: None;
        """
        self.assetFileComboBox.clear()
        self.data['asset_files'] = {}
        self.assetFileComboBox.addItem('')
        selected_version = self.assetVersionComboBox.currentText()
        selected_resource = self.assetResourceComboBox.currentText()
        if selected_version:
            # Get the selected version's files:
            version_entity = self.data['asset_resources'][selected_resource]['versions'][selected_version]
            self.api.query_fields = ['format']
            for file_entity in sorted(version_entity.files, key=lambda x: x.format):
                # Store the file in the context data dictionary for future reference:
                self.data['asset_files'][file_entity.format] = file_entity
                self.assetFileComboBox.addItem(file_entity.format)

    def populate_shot_version_files(self):
        """
        Add files to the assembly asset file UI elements.
        :return: None;
        """
        self.shotFileComboBox.clear()
        self.data['shot_files'] = {}
        selected_version = self.shotVersionComboBox.currentText()
        selected_resource = self.shotResourceComboBox.currentText()
        self.shotFileComboBox.addItem('')
        if selected_version:
            # Get the selected version's files:
            version_entity = self.data['shot_resources'][selected_resource]['versions'][selected_version]
            self.api.query_fields = ['format']
            for file_entity in sorted(version_entity.files, key=lambda x: x.format):
                # Store the file in the context data dictionary for future reference:
                self.data['shot_files'][file_entity.format] = file_entity
                self.shotFileComboBox.addItem(file_entity.format)

    def populate_workspace_resources(self):
        """
        Add resources to the workspace area of the UI.
        :return: None;
        """
        self.resourceList.clear()
        self.versionList.clear()
        self.data['workspace_resources'] = {}

        current_task = self.get_context_task()
        if current_task:
            self.api.query_fields = ['category']
            for resource in sorted(current_task.resources, key=lambda x: x.long_name):
                if resource.category.id == self.dcc.category.id:
                    item = QtWidgets.QListWidgetItem(resource.long_name)
                    self.resourceList.addItem(item)
                    # Store the resource in the context data dictionary for future reference:
                    self.data['workspace_resources'][resource.long_name] = {'entity': resource, 'versions': {}}

    def populate_workspace_versions(self):
        """
        Add the currently selected resource's versions to the workspace area of the UI.
        :return: None;
        """
        self.versionList.clear()

        selected_resource_item = self.resourceList.currentItem()
        if selected_resource_item:
            resource_entity = self.data['workspace_resources'][selected_resource_item.text()]['entity']
            os.environ['PIPER_RESOURCE_ID'] = str(resource_entity.id)
            os.environ['PIPER_VERSION_ID'] = ''
            self.logger.info(msg='Set context resource ID: %s' % resource_entity.id)
            # Get the selected resource's versions:
            self.api.query_fields = ['work_path', 'work_file']
            if resource_entity.versions:
                versions = sorted(resource_entity.versions, key=lambda x: x.long_name, reverse=True)
                for version in versions:
                    # Store the version in the context data dictionary for future reference:
                    self.data['workspace_resources'][selected_resource_item.text()][
                        'versions'][version.long_name] = version
                    item = QtWidgets.QListWidgetItem(version.long_name)
                    self.versionList.addItem(item)
                os.environ['PIPER_VERSION_ID'] = str(versions[0].id)

    def populate_snapshots(self):
        """
        Find all snapshots of the currently selected version and represent them in the workspace area of the UI.
        :return: None;
        """
        self.snapshotList.clear()
        self.data['snapshots'] = {}

        selected_resource_item = self.resourceList.currentItem()
        selected_version_item = self.versionList.currentItem()

        if selected_version_item:
            version_entity = self.data['workspace_resources'][selected_resource_item.text()][
                                        'versions'][selected_version_item.text()]
            os.environ['PIPER_VERSION_ID'] = str(version_entity.id)
            self.logger.info(msg='Set context version ID: %s' % version_entity.id)
            for snapshot, snapshot_path in version_entity.snapshots:
                # Store snapshot in the context data dictionary for future reference:
                self.data['snapshots'][snapshot] = snapshot_path
                self.snapshotList.addItem(snapshot)

    def run_create_resource(self):
        """
        Instantiate the Create Work File widget.
        :return: Error message if lacking valid input, otherwise - None;
        """
        if self.current_task:
            create_resource_dialog = CreateResource(parent=self)
            create_resource_dialog.resource_created.connect(self.on_resource_created)
            create_resource_dialog.exec_()
        else:
            return error_message(parent=self, msg='No task selected!')

    def on_resource_created(self):
        """
        Method to be executed whenever a new resource is created.
        :return: None;
        """
        # Refresh workspace resources and assembly assets:
        self.populate_workspace_resources()
        self.populate_assets()

    def find_asset(self):
        """
        Find the selected asset/resource/version's full publish path and open it in the file explorer if it exists.
        :return: None;
        """
        selected_version = self.assetVersionComboBox.currentText()
        selected_resource = self.assetResourceComboBox.currentText()
        # Determine which entity to locate based on which asset/resource/version is selected:
        if not selected_version:
            if not selected_resource:
                selected_asset = self.assetComboBox.currentText()
                if not selected_asset:
                    return
                else:
                    entity = self.data['assets'][selected_asset]
            else:
                entity = self.data['asset_resources'][selected_resource]['entity']
        else:
            entity = self.data['asset_resources'][selected_resource]['versions'][selected_version]

        self._entity_path_check(entity=entity)

    def find_shot(self):
        """
        Find the selected shot/resource/version's full publish path and open it in the file explorer if it exists.
        :return: None;
        """
        selected_version = self.shotVersionComboBox.currentText()
        selected_resource = self.shotResourceComboBox.currentText()
        # Determine which entity to locate based on which asset/resource/version is selected:
        if not selected_version:
            if not selected_resource:
                return
            else:
                entity = self.data['shot_resources'][selected_resource]['entity']
        else:
            entity = self.data['shot_resources'][selected_resource]['versions'][selected_version]

        self._entity_path_check(entity=entity)

    def _entity_path_check(self, entity):
        """
        Check if an entity's published path exists and open it if it does.
        :param entity: a database entity;
        :return: None;
        """
        path = entity.publish_path
        if os.path.isdir(path):
            browse_path(os.path.abspath(path))
        else:
            self.logger.error("This entity's publish path %s does not exist." % path)

    def open_work_file_version(self):
        """
        Attempt to open the selected work file version inside the current DCC.
        :return: None;
        """
        version_entity = self.api.get(entity_type='Version',
                                      entity_id=int(os.environ['PIPER_VERSION_ID']),
                                      fields=['work_file'])
        os.environ['PIPER_WORK_FILE_ID'] = ''

        if version_entity:
            work_file = version_entity.work_file
            if work_file:
                if work_file.format not in self.dcc.file_extensions:
                    return error_message(parent=self, msg='{0} cannot open "{1}" file types.'.format(
                        self.dcc.name.title(),
                        work_file.format))
                path = work_file.work_path
                # Try to open the version file:
                result = self.dcc_api.open_file(path)
                if not result:
                    return error_message(parent=self, msg='Could not find version file: {}'.format(path))
                os.environ['PIPER_WORK_FILE_ID'] = str(work_file.id)
                self.refresh_tree_data()
            else:
                return error_message(parent=self, msg='Version {} does not have a valid work file.'.format(
                    version_entity.long_name))

    def open_snapshot(self):
        """
        Open the currently selected snapshot file.
        :return: None;
        """
        selected_snapshot_item = self.snapshotList.currentItem()
        if selected_snapshot_item:
            snapshot_name = selected_snapshot_item.text()
            self.dcc_api.open_file(self.data['snapshots'][snapshot_name])
            self.refresh_tree_data()
            os.environ['PIPER_WORK_FILE_ID'] = snapshot_name.split('-snap-')[0].split('_')[-1]

    def _set_widget_signal_block(self, block):
        """
        Block/unblock widget signals.
        :param block: lock boolean;
        :return: None;
        """
        # Block/unblock all relevant widget signals:
        self.assetTypeComboBox.blockSignals(block)
        self.assetComboBox.blockSignals(block)
        self.assetResourceComboBox.blockSignals(block)
        self.resourceList.blockSignals(block)
        self.versionList.blockSignals(block)

    def refresh_ui(self):
        """
        Update all database data contained in various UI elements.
        :return: None;
        """
        # Block widget signals:
        self._set_widget_signal_block(block=True)
        # Repopulate the UI:
        self._setup_data()
        self.logger.debug('UI refreshed.')
        # Unblock all blocked signals:
        self._set_widget_signal_block(block=False)

    def create_snapshot(self):
        """
        Snapshot the current state of the scene file.
        :return:
        """
        self.dcc_api.snapshot()
        self.populate_snapshots()

    def add_assembly_version_file(self, link_type):
        """
        Add the currently selected asset/shot version file to the scene.
        :param link_type: the (str) link type - 'asset' or 'shot';
        :return: An error message if unsuccessful, otherwise - None;
        """
        if link_type == 'asset':
            resource_name = self.assetResourceComboBox.currentText()
            file_name = self.assetFileComboBox.currentText()
        else:
            resource_name = self.shotResourceComboBox.currentText()
            file_name = self.shotFileComboBox.currentText()

        if not file_name:
            return

        resource_type = '{}_resources'.format(link_type)
        file_type = '{}_files'.format(link_type)
        file_entity = self.data[file_type][file_name]
        resource = self.data[resource_type][resource_name]['entity']

        # Check if the corresponding resource is already in the scene:
        if self.dcc_api.find_node_from_id(id_attribute='piper_resource_id', id_value=resource.id):
            return error_message(parent=self, msg='This file is already present!')

        self.dcc_api.add_published_file(file_entity)
        referenced_file_ids = self.dcc_api.find_entity_nodes()
        if file_entity.id not in referenced_file_ids:
            return error_message(parent=self,
                                 msg='There was a problem loading this file.')
        item = self.append_to_assembly_tree(parent_item=self.assemblyTreeWidget, entity=file_entity)
        # Add any child references to the newly added assembly tree item:
        self.build_assembly_tree(referenced_file_ids=referenced_file_ids[file_entity.id], parent_item=item)

    def log_work(self):
        """
        Run the log work dialog.
        :return: None if successful, otherwise - an error message.
        """
        if environment_check('PIPER_TASK_ID'):
            log_work = LogWork(parent=self)
            log_work.exec_()
        else:
            return error_message(parent=self, msg='There is no task currently being worked on!')

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.tasksTableWidget.cellClicked.connect(self.set_task_environment)
        self.tasksTableWidget.cellDoubleClicked.connect(self.on_cell_double_clicked)
        self.assetTypeComboBox.currentIndexChanged.connect(self.populate_assets)
        self.assetComboBox.currentIndexChanged.connect(self.populate_asset_resources)
        self.assetResourceComboBox.currentIndexChanged.connect(self.populate_asset_resource_versions)
        self.assetVersionComboBox.currentIndexChanged.connect(self.populate_asset_version_files)
        self.actionNewResource.triggered.connect(self.run_create_resource)
        self.actionSnapshot.triggered.connect(self.create_snapshot)
        self.actionWorkLog.triggered.connect(self.log_work)
        self.resourceList.currentItemChanged.connect(self.populate_workspace_versions)
        self.resourceList.itemDoubleClicked.connect(self.open_work_file_version)
        self.versionList.itemDoubleClicked.connect(self.open_work_file_version)
        self.versionList.currentItemChanged.connect(self.populate_snapshots)
        self.snapshotList.itemDoubleClicked.connect(self.open_snapshot)
        self.findAssetBtn.clicked.connect(self.find_asset)
        self.findShotBtn.clicked.connect(self.find_shot)
        self.refreshBtn.clicked.connect(self.refresh_ui)
        self.assetVersionAddBtn.clicked.connect(lambda: self.add_assembly_version_file(link_type='asset'))
        self.shotVersionAddBtn.clicked.connect(lambda: self.add_assembly_version_file(link_type='shot'))
        self.shotResourceComboBox.currentIndexChanged.connect(self.populate_shot_resource_versions)
        self.shotVersionComboBox.currentIndexChanged.connect(self.populate_shot_version_files)
        self.closeBtn.clicked.connect(self.close)


def run():
    """
    Create and run the application.
    :return: None;
    """
    app = QtWidgets.QApplication(sys.argv)
    show()
    sys.exit(app.exec_())


def show(parent=None):
    """
    Show the UI.
    :param parent: parent Qt window;
    :return: None;
    """
    sys.excepthook = piper_except_hook
    scene_builder = SceneBuilder(parent)
    scene_builder.show()


if __name__ == '__main__':
    run()
