# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
import tempfile
from datetime import datetime
from piper_client.gui import MainWindow
from piper_client.gui.qt import QtGui, QtCore, QtWidgets
from piper_client.gui.utils import info_message, error_message, warning_message, load_qt_ui, set_combo_box_text
from piper_client.core.api import PiperClientApi
from piper_client.core.utils import piper_except_hook, environment_check, run_in_terminal
from piper_client.core.renderfarm import load_render_farm_api, FarmJob, RENDER_CALLBACK_TEMPLATE
from piper_client.core.dcc import load_dcc_api


class RenderFarmer(MainWindow):
    """
    A dialog for setting up a farm submission.
    """

    def __init__(self, parent, dcc_api=None):
        MainWindow.__init__(self, parent, 'Piper Render Farmer', 'render_farmer.ui')
        self.api = PiperClientApi()
        self.version = self.api.get(entity_type='Version', entity_id=int(os.environ['PIPER_VERSION_ID']))
        self.dcc = self.api.get(entity_type='Software', entity_id=int(os.environ['PIPER_SOFTWARE_ID']),
                                fields=['render_formats'])
        self.dcc_api = dcc_api
        if not self.dcc_api:
            dcc_api = load_dcc_api(os.environ['PIPER_DCC'])
            self.dcc_api = dcc_api()
        self.data = {'farms': {}}
        self._setup_data()
        self._connect_widget_cmds()
        self.status = None
        self.file_entity = None
        self.farm = None

    def _setup_data(self):
        """
        Assign data to UI elements.
        :return: None;
        """
        self.renderFarmComboBox.addItem('')
        farms = self.api.get(entity_type='Software', filters={'is_farm': True}, fields=['command_line_renderer',
                                                                                        'system'])
        for farm in farms:
            farm_name = '{0} on {1}'.format(farm.long_name, farm.system.long_name)
            self.renderFarmComboBox.addItem(farm_name)
            self.data['farms'][farm_name] = farm

        self.formatComboBox.addItem('')
        self.formatComboBox.addItems(self.dcc.render_formats)
        current_format = self.dcc_api.get_image_format()
        if current_format:
            set_combo_box_text(self.formatComboBox, current_format)

        self.renderNodeComboBox.addItem('')
        self.renderNodeComboBox.addItems(self.dcc_api.list_nodes())

        self.jobNameLineEdit.setText('{0} | {1}'.format(os.environ['PIPER_DCC'],
                                                        self.version.get(attribute='work_path')))
        start_frame = self.dcc_api.get_render_start_frame() or 1
        self.startFrameSpinBox.setValue(start_frame)
        self.endFrameSpinBox.setValue(self.dcc_api.get_render_end_frame() or start_frame + 10)
        self.byFrameSpinBox.setValue(1)
        self.retryAttemptsSpinBox.setValue(1)
        self.status = self.api.get_one(entity_type='Status', filters={'name': 'rendering'})

        
    def setup_render(self):
        format = self.formatComboBox.currentText()
        if not format:
            return error_message(parent=self, msg='No render format selected.')

        # Create a version file to represent the render:
        self.file_entity = self.api.create(entity_type='File',
                                           version=self.version,
                                           artist=self.version.artist,
                                           is_work_file=False,
                                           status=self.status,
                                           is_sequence=True,
                                           format=format)
        self.dcc_api.render_setup(file_entity=self.file_entity)
        self.dcc_api.save_file()
        # Set the render path to the file's work directory on the render farm OS:
        if self.farm:
            render_path = os.path.join(self.farm.system.work_root,
                                       os.path.dirname(self.file_entity.get(attribute='work_path')))
        else:
            render_path = self.file_entity.work_path
        self.dcc_api.set_render_path(path=render_path)
        self.setupRenderBtn.setEnabled(False)

    def submit(self):
        if not self.dcc_api.software.command_line_renderer:
            return error_message(parent=self,
                                 msg='{} does not have a configured command line renderer, aborting.'.format(
                                     self.dcc_api.software.long_name))

        if not environment_check('PIPER_WORK_FILE_ID'):
            return error_message(parent=self, msg='No work file currently open.')

        scene_name = self.dcc_api.get_current_file_name()
        if not scene_name:
            return error_message(parent=self, msg='Invalid file work path.')

        work_file = self.api.get(entity_type='File', entity_id=int(os.environ['PIPER_WORK_FILE_ID']))
        if not work_file:
            return error_message(parent=self, msg='Could not find current work file entity.')

        send_to_farm = self.renderFarmRadioButton.isChecked()

        if send_to_farm and not self.farm:
            return error_message(parent=self, msg='No render farm selected.')
        if send_to_farm and not self.farm.command_line_renderer:
            return error_message(parent=self,
                                 msg='{} does not have a configured command line renderer, aborting.'.format(
                                     self.farm.long_name))

        # Create a version file to represent the render:
        if not self.file_entity:
            self.setup_render()
        if not self.file_entity:
            return error_message(parent=self, msg='Render setup failed.')

        # Gather job data from the UI:
        callback = self.pythonCallbackLineEdit.text()
        flags = self.additionalFlagsLineEdit.text()
        description = self.descriptionTextEdit.toPlainText()
        retry_attempts = self.retryAttemptsSpinBox.value()
        instances = self.instancesSpinBox.value()
        start_frame = self.startFrameSpinBox.value()
        end_frame = self.endFrameSpinBox.value()
        by_frame = self.byFrameSpinBox.value()
        name = self.jobNameLineEdit.text()
        publish = self.publishCheckBox.isChecked()
        priority = self.api.get_top_configuration(entity=self.version, attribute='priority') or 1

        if send_to_farm:
            scene = os.path.join(self.farm.system.work_root, os.path.dirname(work_file.get(attribute='work_path')))

            # Submit the render farm job:
            render_farm_api = load_render_farm_api(self.farm.name)
            farm_job = render_farm_api(version=self.version, file=self.file_entity, farm=self.farm,
                                       start_frame=start_frame, end_frame=end_frame, by_frame=by_frame,
                                       cpus=instances, retry_attempts=retry_attempts, priority=priority, name=name,
                                       description=description, publish=publish, callback=callback,
                                       flags=flags, scene=scene, dcc_api=self.dcc_api)
            farm_job.submit()

            info_message(parent=self, msg='Job submitted to render farm.')

        else:
            executable = os.path.join(self.dcc_api.software.system.software_root,
                                      self.dcc_api.software.command_line_renderer)
            if not os.path.exists(executable):
                return error_message(parent=self,
                                     msg='Could not find executable: {}'.format(executable))

            timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
            job = FarmJob(version=self.version, file=self.file_entity, platform=sys.platform)

            callback_file = os.path.join(tempfile.gettempdir(), '_piper_deadline_callback_{}.py'.format(timestamp))
            piper_callback = RENDER_CALLBACK_TEMPLATE.format(FILE_ID=self.file_entity.id, ADDITIONAL_CALLBACK=callback)
            with open(callback_file, 'w+') as tmp_file:
                tmp_file.write(piper_callback)
            scene = work_file.work_path
            render_flags = '{0} {1}'.format(self.dcc_api.render_flags.format(
                                            START_FRAME=start_frame,
                                            END_FRAME=end_frame,
                                            BY_FRAME=by_frame,
                                            SCENE=scene,
                                            RENDER_NODE=self.dcc_api.render_node),
                                            flags)
            job.command += '"{0}" {1} && python {2}'.format(executable,
                                                            render_flags,
                                                            callback_file)
            self.logger.info('Submitting local job: %s' % job.command)
            run_in_terminal(job.command)

            info_message(parent=self, msg='Local render job submitted.')

        # Update the version status appropriately:
        self.version['status'] = self.status
        self.version.update()
        self.close()

    def _render_locally_check(self):
        state = self.renderFarmRadioButton.isChecked()
        self.renderFarmComboBox.setEnabled(state)
        self.instancesSpinBox.setEnabled(state)
        self.retryAttemptsSpinBox.setEnabled(state)
        self.instancesSpinBox.setEnabled(state)
        self.descriptionTextEdit.setEnabled(state)

    def _on_format_changed(self):
        format = self.formatComboBox.currentText()
        if format and self.file_entity:
            self.file_entity['format'] = format
            self.file_entity.update()

    def _on_farm_selected(self):
        render_farm_name = self.renderFarmComboBox.currentText()
        if render_farm_name:
            self.farm = self.data['farms'][render_farm_name]

    def _on_render_node_selected(self):
        self.dcc_api.render_node = self.renderNodeComboBox.currentText()

    def _connect_widget_cmds(self):
        """
        Connect UI elements' signals to the appropriate methods.
        :return: None;
        """
        self.cancelBtn.clicked.connect(self.close)
        self.submitBtn.clicked.connect(self.submit)
        self.setupRenderBtn.clicked.connect(self.setup_render)
        self.formatComboBox.currentIndexChanged.connect(self._on_format_changed)
        self.renderLocalRadioButton.toggled.connect(self._render_locally_check)
        self.renderFarmComboBox.currentIndexChanged.connect(self._on_farm_selected)
        self.renderNodeComboBox.currentIndexChanged.connect(self._on_render_node_selected)


def show(parent=None):
    """
    Show the render farmer dialog.
    :param parent: parent Qt window;
    :return: None;
    """
    sys.excepthook = piper_except_hook
    if not parent:
        parent = QtWidgets.QApplication.activeWindow()
    if environment_check('PIPER_VERSION_ID'):
        render_farmer = RenderFarmer(parent=parent)
        render_farmer.show()
    else:
        msg = 'Piper Render Farmer: no version is currently being worked on.'
        error_message(parent=parent, msg=msg)
        raise EnvironmentError(msg)
