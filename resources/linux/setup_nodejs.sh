#!/bin/bash
echo Setting up NodeJS...
curl --remote-name http://piperpipeline.com/resources/linux/node-v10.15.3-linux-x64.tar.xz
tar -xzvf node-v10.15.3-linux-x64.tar.xz && mv node-v10.15.3-linux-x64 NodeJS
echo NodeJS has been set up.