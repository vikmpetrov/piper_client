# Piper Server
Piper Client is desktop application component of the Piper Pipeline Toolkit - an out-of-the-box CGI production tracking 
and data management software suite.

Developed and maintained by [Viktor Petrov](https://gitlab.com/vikmpetrov), Piper I/O EOOD.

## Version
1.0.1

## Setup

*Prerequisites*: 
- Download and install [Python](https://www.python.org/) (version 3.6 or above);
- Download and install [Node.js](https://nodejs.org/en/download/);

### On Windows:
Navigate to this directory and run the following script:
```
.\setup.bat
```

### On Linux:
Open a terminal shell, navigate to this directory and run the following script:
```
source setup.sh
```

### On Mac:
Open a terminal shell, navigate to this directory and run the following script:
```
source setup.command
```

## API

Using the Piper Client API:

```
from piper_client.core.api import PiperClientApi

piper_api = PiperClientApi()
print(dir(piper_api))

# Query entities example:
projects = piper_api.get_entities(entity='Project')
```

## License

This software package's EULA is outlined in its [LICENSE.md](LICENSE.md) file.

## Copyright
&copy; Piper I/O EOOD. All rights reserved.
