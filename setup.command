echo Setting up Piper Client, please wait...
export PIPER=$PWD
export PIPER_OS=darwin
source $PIPER/resources/darwin/setup_python.command
source $PIPER/resources/darwin/setup_nodejs.command
$PIPER/resources/darwin/Python37/bin/python -m virtualenv venv_darwin -p $PIPER/resources/darwin/Python37/bin/python
source venv_darwin/bin/activate
pip install -r requirements.txt
cd apps/launcher
npm install .
npm run package-mac
cd $PIPER
echo Piper Client setup completed.