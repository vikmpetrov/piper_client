# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os

DCC_API_MODULE_PATH = os.path.abspath(os.path.join(os.environ['PIPER'], 'core', 'dcc'))  # path to the DCC APIs;

# The template for Maya menu creation:
DCC_MENU_TEMPLATE = {
            'main': {
                'Browser': 'import piper_client.apps.browser as piper_browser;reload(piper_browser);\
                            piper_browser.show()',
                'Scene Builder': 'import piper_client.apps.scene_builder as piper_scene_builder;\
                                 reload(piper_scene_builder);piper_scene_builder.show()',
                'Publisher': 'import piper_client.apps.publisher as piper_publisher;reload(piper_publisher); \
                              piper_publisher.show()'
            },
            'utilities': {
                'Snapshot': 'from piper_client.core.dcc.maya_api import PiperDccApi;dcc_api=PiperDccApi();\
                             dcc_api.snapshot()',
                'Log Work': 'import piper_client.gui.widgets.log_work as piper_log_work;reload(piper_log_work);\
                             piper_log_work.show()',
                'Cascading Config': 'import piper_client.gui.widgets.cascading_config as piper_cascading_config; \
                                     reload(piper_cascading_config);piper_cascading_config.show()',
                'Render Farmer': 'import piper_client.apps.render_farmer as piper_render_farmer; \
                     reload(piper_render_farmer);piper_render_farmer.show();'
            },
            'links': {
                'Current Entity Page': 'from piper_client.core.api import PiperClientApi;\
                                        piper_api = PiperClientApi();piper_api.open_current_entity_page();'
            }
    }
